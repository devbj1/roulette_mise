var loader = '<div class="loader text-center mt-3 mb-3"><i class="fa fa-spinner fa-spin fa-3x"></i></div>';


var table = $('#tablelistetirage').DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun tirage disponible",
        "sEmptyTable": "Aucun tirage disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucun tirage disponible",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant",
            "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});

var table1 = $('#tablelistefilleul').DataTable({
    //'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun filleul disponible",
        "sEmptyTable": "Aucun filleul disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucun filleul disponible",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant",
            "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});

var table2 = $('#tablelistehistorique').DataTable({
    //'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun historique disponible",
        "sEmptyTable": "Aucun historique disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucun historique disponible",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant",
            "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});


var btncopy = document.querySelector('.js-copy');
if(btncopy) {
    btncopy.addEventListener('click', docopy);
}

function docopy() {

    // Cible de l'élément qui doit être copié
    var target = this.dataset.target;
    var fromElement = document.querySelector(target);
    if(!fromElement) return;

    // Sélection des caractères concernés
    var range = document.createRange();
    var selection = window.getSelection();
    range.selectNode(fromElement);
    selection.removeAllRanges();
    selection.addRange(range);

    try {
        // Exécution de la commande de copie
        var result = document.execCommand('copy');
        if (result) {
            // La copie a réussi
            //alert('Copié !');
            $.notify('Lien de parrainage copié !', 'success');

        }
    }
    catch(err) {
        // Une erreur est surevnue lors de la tentative de copie
        //alert(err);
        $.notify('Veuillez réessayer !', 'error');

    }

    // Fin de l'opération
    selection = window.getSelection();
    if (typeof selection.removeRange === 'function') {
        selection.removeRange(range);
    } else if (typeof selection.removeAllRanges === 'function') {
        selection.removeAllRanges();
    }
}



$('.leticket').click(function (e) {
    e.preventDefault();
    var tick = $(this);
// console.log("okssd");
    var hreflienorigine = "/idtirage/idticket/paiementsolde";

    var hreflienorigine1 = "/idtirage/idticket/paiementpayeer";

    var res = hreflienorigine.replace("idticket", tick.attr('data-value'));

    var res1 = hreflienorigine1.replace("idticket", tick.attr('data-value'));

    var res2 = res.replace("idtirage", $('#ntirage').attr('data-value'));

    var res3 = res1.replace("idtirage", $('#ntirage').attr('data-value'));

    $('#btn_paiement_solde').attr('href', res2);

    $('#btn_paiement_payeer').attr('href', res3);


});


$(".confirmationpayer").click(function (e) {
    e.preventDefault();

    var btntirage = $(this);

    var loaderPanel = $("#loaderecherchelisteticket");

    var listeTicket = $("#tablelignegagnant");

    //loaderPanel.html(loader);

    btntirage.button('loading');

    //console.log("ok");
    var idTirage = btntirage.attr('data-id');
    // console.log(agent.val(),service.val(),delai.val())
    if (idTirage === '0') {
        idTirage.notify(
            "Veuillez selectionner le service",
            {position: "right"},
            "warn"
        );
    } else {
        //console.log(idTirage);
        $.ajax({
            url: listeTicketGagnantAPayerUrl,
            type: "POST",
            data: {
                'idTirage': idTirage,
            },
            success: function (data) {

                //console.log(data);
                //loaderPanel.html("");
                listeTicket.html(data).show(5000);


                $('.btn_payer').click(function (e) {
                    e.preventDefault();

                    var btnpayer = $(this);

                    //console.log("ok");
                    //loaderPanel.html(loader);

                    btnpayer.button('loading');

                    //console.log("ok");
                    var idTicketClient = btnpayer.attr('data-idtirageclient');
                    // console.log(agent.val(),service.val(),delai.val())
                    if (idTicketClient === '0') {
                        idTicketClient.notify(
                            "Veuillez selectionner tirage ticket",
                            {position: "right"},
                            "warn"
                        );
                    } else {
                        //console.log(idTirage);
                        $.ajax({
                            url: payerUrl,
                            type: "POST",
                            data: {
                                'idTirageClient': idTicketClient,
                            },
                            success: function (data) {

                                // console.log(data['statut']);

                                if (data['statut'] = 'success'){
                                    $.notify('Paiement effectué avec succes !', 'success');

                                    btnpayer.closest('tr').fadeOut(1000, function () {
                                        $(this).remove();
                                    });
                                }else{
                                    $.notify('Erreur lors du paiement veuillez réessayer!', 'success');

                                }

                                //loaderPanel.html("");
                                // listeTicket.html(data).fadeIn('slow');


                            },
                            error: function (error) {

                            }

                        })
                    }


                })
                ;

            },
            error: function (error) {

            }

        })
    }


})
;



function payeer() {

    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-vous vraiment dupliquer cette consultation ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            btn.button('loading');
            idpat.addClass('disabled');
            $.ajax({
                url: dupliqueConsultationUrl,
                type: "POST",
                data: {
                    'idpatient': idpat.val(),
                    'idconsultation': idConsultationADuplique,
                },
                success: function (data) {

                    if (data['success'] === true) {
                        swalWithBootstrapButtons.fire(
                            'Dupliquer!',
                            'Opération effectuée avec success !',
                            'success'
                        );


                    } else {
                        swalWithBootstrapButtons.fire(
                            'Enregistrement!',
                            'Echec de l\'enregistrement de la consultation!',
                            'error'
                        )

                        //$.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                    }

                },
                error: function (error) {

                }
            }).always(function () {
                btn.button('reset');
                idpat.removeClass('disabled');

            });

        }
    });
}

// var table1 = $('#tablelistetirage').DataTable({
//     //'order': [[0, 'desc']],
//     "language": {
//         "sProcessing": "Traitement en cours ...",
//         "sLengthMenu": "Afficher _MENU_ lignes",
//         "sZeroRecords": "Aucun tirage disponible",
//         "sEmptyTable": "Aucun tirage disponible",
//         "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
//         "sInfoEmpty": "Aucun tirage disponible",
//         "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
//         "sInfoPostFix": "",
//         "sSearch": "Chercher:",
//         "sUrl": "",
//         "sInfoThousands": ",",
//         "sLoadingRecords": "Chargement...",
//         "oPaginate": {
//             "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
//         },
//         "oAria": {
//             "sSortAscending": ": Trier par ordre croissant",
//             "sSortDescending": ": Trier par ordre décroissant"
//         }
//     }
//
// });

//
// var table1 = $('#tablelisteventes').DataTable({
//     //'order': [[0, 'desc']],
//     "language": {
//         "sProcessing": "Traitement en cours ...",
//         "sLengthMenu": "Afficher _MENU_ lignes",
//         "sZeroRecords": "Liste des l'articles a vendre",
//         "sEmptyTable": "Liste des l'articles a vendre",
//         "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
//         "sInfoEmpty": "Liste des l'articles a vendre",
//         "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
//         "sInfoPostFix": "",
//         "sSearch": "Chercher:",
//         "sUrl": "",
//         "sInfoThousands": ",",
//         "sLoadingRecords": "Chargement...",
//         "oPaginate": {
//             "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
//         },
//         "oAria": {
//             "sSortAscending": ": Trier par ordre croissant",
//             "sSortDescending": ": Trier par ordre décroissant"
//         }
//     }
//
// });



