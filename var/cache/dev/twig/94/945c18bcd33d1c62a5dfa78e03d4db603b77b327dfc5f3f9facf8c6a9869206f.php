<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* template/header.html.twig */
class __TwigTemplate_c315de0cc9c19b1c44e2762948fe3437fda2b0803a375fcf8ffb19a0f453d341 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "template/header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "template/header.html.twig"));

        // line 1
        echo "

<!-- Navbar Search -->
<form class=\"d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0\">
    <div class=\"input-group\" hidden>
        <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\" aria-label=\"Search\" aria-describedby=\"basic-addon2\">
        <div class=\"input-group-append\">
            <button class=\"btn btn-primary\" type=\"button\">
                <i class=\"fas fa-search\"></i>
            </button>
        </div>
    </div>
</form>

<!-- Navbar -->
<ul class=\"navbar-nav ml-auto ml-md-0 pull-left\">
    ";
        // line 41
        echo "    <div id=\"showtime\" style=\"color: #cbd3da; font-size: 25px\" class=\"pull-right\"></div>
    <li class=\"nav-item dropdown no-arrow\">
        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"userDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <i class=\"fas fa-user-circle fa-fw\"></i>
        </a>
        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"userDropdown\">
            ";
        // line 50
        echo "

            <a class=\"dropdown-item\" href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("out");
        echo "\">Logout</a>
        </div>
    </li>
</ul>


";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "template/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 52,  69 => 50,  61 => 41,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("

<!-- Navbar Search -->
<form class=\"d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0\">
    <div class=\"input-group\" hidden>
        <input type=\"text\" class=\"form-control\" placeholder=\"Search for...\" aria-label=\"Search\" aria-describedby=\"basic-addon2\">
        <div class=\"input-group-append\">
            <button class=\"btn btn-primary\" type=\"button\">
                <i class=\"fas fa-search\"></i>
            </button>
        </div>
    </div>
</form>

<!-- Navbar -->
<ul class=\"navbar-nav ml-auto ml-md-0 pull-left\">
    {#<li class=\"nav-item dropdown no-arrow mx-1\">
        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"alertsDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <i class=\"fas fa-bell fa-fw\"></i>
            <span class=\"badge badge-danger\">9+</span>
        </a>
        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"alertsDropdown\">
            <a class=\"dropdown-item\" href=\"#\">Action</a>
            <a class=\"dropdown-item\" href=\"#\">Another action</a>
            <div class=\"dropdown-divider\"></div>
            <a class=\"dropdown-item\" href=\"#\">Something else here</a>
        </div>
    </li>
    <li class=\"nav-item dropdown no-arrow mx-1\">
        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"messagesDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <i class=\"fas fa-envelope fa-fw\"></i>
            <span class=\"badge badge-danger\">7</span>
        </a>
        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"messagesDropdown\">
            <a class=\"dropdown-item\" href=\"#\">Action</a>
            <a class=\"dropdown-item\" href=\"#\">Another action</a>
            <div class=\"dropdown-divider\"></div>
            <a class=\"dropdown-item\" href=\"#\">Something else here</a>
        </div>
    </li>#}
    <div id=\"showtime\" style=\"color: #cbd3da; font-size: 25px\" class=\"pull-right\"></div>
    <li class=\"nav-item dropdown no-arrow\">
        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"userDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <i class=\"fas fa-user-circle fa-fw\"></i>
        </a>
        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"userDropdown\">
            {#<a class=\"dropdown-item\" href=\"#\">Settings</a>
            <a class=\"dropdown-item\" href=\"#\">Activity Log</a>
            <div class=\"dropdown-divider\"></div>#}


            <a class=\"dropdown-item\" href=\"{{ path('out') }}\">Logout</a>
        </div>
    </li>
</ul>


", "template/header.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\template\\header.html.twig");
    }
}
