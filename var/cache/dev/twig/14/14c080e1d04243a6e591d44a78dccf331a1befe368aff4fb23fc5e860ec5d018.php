<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* tirage/index.html.twig */
class __TwigTemplate_4e4d55acd5b39abf0a3132920d4fc93bbeb056f8ab8123d03978c6153c7590bb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return ":tirage:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tirage/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tirage/index.html.twig"));

        $this->parent = $this->loadTemplate(":tirage:template.html.twig", "tirage/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "

    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste des tirages en cours
            <a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage_new");
        echo "\" class=\"btn btn-primary\" style=\"\">Créer un nouveau tirage</a>
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\">N° Tirage</th>
                        <th>Cout tirage</th>
                        <th>Nbr numero</th>
                        <th>Nbr numero Gagnant</th>
                        <th>Etat tirage</th>
                        <th>Date tirage</th>
                        ";
        // line 27
        echo "                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tirages"]) || array_key_exists("tirages", $context) ? $context["tirages"] : (function () { throw new RuntimeError('Variable "tirages" does not exist.', 31, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["tirage"]) {
            // line 32
            echo "                        <tr>
                            <td>#";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirage"], "id", [], "any", false, false, false, 33), "html", null, true);
            echo "</td>
                            <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirage"], "coutTirage", [], "any", false, false, false, 34), "html", null, true);
            echo "</td>
                            <td>";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirage"], "nbrNumero", [], "any", false, false, false, 35), "html", null, true);
            echo "</td>
                            <td>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirage"], "nbrNumeroGagnant", [], "any", false, false, false, 36), "html", null, true);
            echo "</td>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirage"], "etatTirage", [], "any", false, false, false, 37), "html", null, true);
            echo "</td>
                            <td>";
            // line 38
            if (twig_get_attribute($this->env, $this->source, $context["tirage"], "dateTirage", [], "any", false, false, false, 38)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirage"], "dateTirage", [], "any", false, false, false, 38), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                            ";
            // line 40
            echo "                                ";
            // line 41
            echo "                                    ";
            // line 42
            echo "                                        ";
            // line 43
            echo "                                    ";
            // line 44
            echo "                                    ";
            // line 45
            echo "                                        ";
            // line 46
            echo "                                    ";
            // line 47
            echo "                                ";
            // line 48
            echo "                            ";
            // line 49
            echo "                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tirage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 61
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 62
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\" src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("front/tirage/js/index.js"), "html", null, true);
        echo "\"></script>

    ";
        // line 69
        echo "    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "tirage/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 69,  190 => 64,  184 => 62,  174 => 61,  158 => 51,  151 => 49,  149 => 48,  147 => 47,  145 => 46,  143 => 45,  141 => 44,  139 => 43,  137 => 42,  135 => 41,  133 => 40,  127 => 38,  123 => 37,  119 => 36,  115 => 35,  111 => 34,  107 => 33,  104 => 32,  100 => 31,  94 => 27,  78 => 13,  69 => 6,  59 => 5,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends ':tirage:template.html.twig' %}



{% block content %}


    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste des tirages en cours
            <a href=\"{{ path('tirage_new') }}\" class=\"btn btn-primary\" style=\"\">Créer un nouveau tirage</a>
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\">N° Tirage</th>
                        <th>Cout tirage</th>
                        <th>Nbr numero</th>
                        <th>Nbr numero Gagnant</th>
                        <th>Etat tirage</th>
                        <th>Date tirage</th>
                        {#<th>Actions</th>#}
                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    {% for tirage in tirages %}
                        <tr>
                            <td>#{{ tirage.id }}</td>
                            <td>{{ tirage.coutTirage }}</td>
                            <td>{{ tirage.nbrNumero }}</td>
                            <td>{{ tirage.nbrNumeroGagnant }}</td>
                            <td>{{ tirage.etatTirage }}</td>
                            <td>{% if tirage.dateTirage %}{{ tirage.dateTirage|date('Y-m-d H:i:s') }}{% endif %}</td>
                            {#<td>#}
                                {#<ul>#}
                                    {#<li>#}
                                        {#<a href=\"{{ path('tirage_show', { 'id': tirage.id }) }}\">show</a>#}
                                    {#</li>#}
                                    {#<li>#}
                                        {#<a href=\"{{ path('tirage_edit', { 'id': tirage.id }) }}\">edit</a>#}
                                    {#</li>#}
                                {#</ul>#}
                            {#</td>#}
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

{% endblock %}



{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\" src=\"{{ asset('front/tirage/js/index.js') }}\"></script>

    {#    #}{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>#}{#
        <script src=\"{{ asset('template/js/modernizr-2.8.3.min.js') }}\"></script>
        <script src=\"{{ asset('template/chart/Chart.min.js') }}\"></script>#}
    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


{% endblock %}
", "tirage/index.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\tirage\\index.html.twig");
    }
}
