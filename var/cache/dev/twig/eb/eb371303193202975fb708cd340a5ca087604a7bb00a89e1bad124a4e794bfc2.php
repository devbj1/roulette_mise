<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* template/sidemenu.html.twig */
class __TwigTemplate_72f3097bbecd573bc4d73bf2d70f8621906539ca96932e45f410e37dc3da1640 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "template/sidemenu.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "template/sidemenu.html.twig"));

        // line 1
        echo "<!-- Sidebar user panel (optional) -->
";
        // line 10
        echo "

<li class=\"nav-item active\">
    <a class=\"nav-link bg-info\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">
        <i class=\"fas fa-fw fa-th\"></i>
        <span>Accueil</span>
    </a>
</li>
<li class=\"nav-item active\">
    <a class=\"nav-link bg-success\" href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage");
        echo "\">
        <i class=\"fas fa-fw fa-plus\"></i>
        <span>Participer a un tirage</span>
    </a>
</li>

";
        // line 25
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "session", [], "any", false, false, false, 25), "get", [0 => "idPayeer"], "method", false, false, false, 25) == "P1026189037")) {
            // line 26
            echo "
    <li class=\"nav-item active\">
        <a class=\"nav-link bg-transparent\" href=\"";
            // line 28
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage_index");
            echo "\">
            <i class=\"fas fa-fw fa-plus\"></i>
            <span>Gestion des Tirages</span>
        </a>
    </li>


    <li class=\"nav-item active\">
        <a class=\"nav-link bg-transparent\" href=\"";
            // line 36
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("notifier_gagnant");
            echo "\">
            <i class=\"fas fa-fw fa-plus\"></i>
            <span>Gestion des Paiements</span>
        </a>
    </li>


    <li class=\"nav-item active\">
        <a class=\"nav-link bg-transparent\" href=\"";
            // line 44
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("recharge_new");
            echo "\">
            <i class=\"fas fa-fw fa-plus\"></i>
            <span>Recharger</span>
        </a>
    </li>

";
        }
        // line 51
        echo "
<li class=\"nav-item active\">
    <a class=\"nav-link bg-secondary\" href=\"";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage_my_index");
        echo "\">
        <i class=\"fas fa-fw fa-chart-area\"></i>
        <span>Point de mes tirages</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-info\" href=\"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("transfert_new");
        echo "\">
        <i class=\"fas fa-fw fa-share\"></i>
        <span>Transferer</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-info\" href=\"";
        // line 67
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("filleuls");
        echo "\">
        <i class=\"fas fa-fw fa-network-wired\"></i>
        <span>Mes filleuls</span>
    </a>
</li>

";
        // line 74
        echo "
    ";
        // line 76
        echo "        ";
        // line 77
        echo "            ";
        // line 78
        echo "            ";
        // line 79
        echo "        ";
        // line 80
        echo "    ";
        // line 81
        echo "
";
        // line 83
        echo "

<li class=\"nav-item active\">
    <a class=\"nav-link bg-dark\" href=\"";
        // line 86
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("historiques");
        echo "\">
        <i class=\"fas fa-fw fa-book\"></i>
        <span>Historiques</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-dark\" href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("faq");
        echo "\">
        <i class=\"fas fa-fw fa-pen\"></i>
        <span>FaQs</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-danger\" href=\"";
        // line 100
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("out");
        echo "\">
        <i class=\"fas fa-fw fa-sign-out-alt\"></i>
        <span>Deconnexion</span>
    </a>
</li>
";
        // line 131
        echo "
<!-- Sidebar Menu -->
";
        // line 163
        echo "        ";
        // line 164
        echo "            ";
        // line 165
        echo "                ";
        // line 166
        echo "                ";
        // line 167
        echo "                    ";
        // line 168
        echo "                    ";
        // line 169
        echo "                ";
        // line 170
        echo "            ";
        // line 171
        echo "            ";
        // line 172
        echo "                ";
        // line 173
        echo "                    ";
        // line 174
        echo "                        ";
        // line 175
        echo "                        ";
        // line 176
        echo "                    ";
        // line 177
        echo "                ";
        // line 178
        echo "                ";
        // line 179
        echo "                    ";
        // line 180
        echo "                        ";
        // line 181
        echo "                        ";
        // line 182
        echo "                    ";
        // line 183
        echo "                ";
        // line 184
        echo "                ";
        // line 185
        echo "                    ";
        // line 186
        echo "                        ";
        // line 187
        echo "                        ";
        // line 188
        echo "                    ";
        // line 189
        echo "                ";
        // line 190
        echo "            ";
        // line 191
        echo "        ";
        // line 192
        echo "        ";
        // line 193
        echo "            ";
        // line 194
        echo "                ";
        // line 195
        echo "                ";
        // line 196
        echo "                    ";
        // line 197
        echo "                    ";
        // line 198
        echo "                ";
        // line 199
        echo "            ";
        // line 200
        echo "        ";
        // line 201
        echo "    ";
        // line 203
        echo "<!-- /.sidebar-menu -->";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "template/sidemenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 203,  269 => 201,  267 => 200,  265 => 199,  263 => 198,  261 => 197,  259 => 196,  257 => 195,  255 => 194,  253 => 193,  251 => 192,  249 => 191,  247 => 190,  245 => 189,  243 => 188,  241 => 187,  239 => 186,  237 => 185,  235 => 184,  233 => 183,  231 => 182,  229 => 181,  227 => 180,  225 => 179,  223 => 178,  221 => 177,  219 => 176,  217 => 175,  215 => 174,  213 => 173,  211 => 172,  209 => 171,  207 => 170,  205 => 169,  203 => 168,  201 => 167,  199 => 166,  197 => 165,  195 => 164,  193 => 163,  189 => 131,  181 => 100,  171 => 93,  161 => 86,  156 => 83,  153 => 81,  151 => 80,  149 => 79,  147 => 78,  145 => 77,  143 => 76,  140 => 74,  131 => 67,  121 => 60,  111 => 53,  107 => 51,  97 => 44,  86 => 36,  75 => 28,  71 => 26,  69 => 25,  60 => 19,  51 => 13,  46 => 10,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Sidebar user panel (optional) -->
{#<div class=\"user-panel mt-3 pb-3 mb-3 d-flex\">
    <div class=\"image\">
        <img src=\"{{ asset('template/dist/img/user2-160x160.jpg') }}\" class=\"img-circle elevation-2\" alt=\"User Image\">
    </div>
    <div class=\"info\">
        <a href=\"#\" class=\"d-block\">{{ app.session.get('nom') }}</a>
    </div>
</div>#}


<li class=\"nav-item active\">
    <a class=\"nav-link bg-info\" href=\"{{ path('homepage') }}\">
        <i class=\"fas fa-fw fa-th\"></i>
        <span>Accueil</span>
    </a>
</li>
<li class=\"nav-item active\">
    <a class=\"nav-link bg-success\" href=\"{{ path('tirage') }}\">
        <i class=\"fas fa-fw fa-plus\"></i>
        <span>Participer a un tirage</span>
    </a>
</li>

{% if app.session.get('idPayeer') == 'P1026189037' %}

    <li class=\"nav-item active\">
        <a class=\"nav-link bg-transparent\" href=\"{{ path('tirage_index') }}\">
            <i class=\"fas fa-fw fa-plus\"></i>
            <span>Gestion des Tirages</span>
        </a>
    </li>


    <li class=\"nav-item active\">
        <a class=\"nav-link bg-transparent\" href=\"{{ path('notifier_gagnant') }}\">
            <i class=\"fas fa-fw fa-plus\"></i>
            <span>Gestion des Paiements</span>
        </a>
    </li>


    <li class=\"nav-item active\">
        <a class=\"nav-link bg-transparent\" href=\"{{ path('recharge_new') }}\">
            <i class=\"fas fa-fw fa-plus\"></i>
            <span>Recharger</span>
        </a>
    </li>

{% endif %}

<li class=\"nav-item active\">
    <a class=\"nav-link bg-secondary\" href=\"{{ path('tirage_my_index') }}\">
        <i class=\"fas fa-fw fa-chart-area\"></i>
        <span>Point de mes tirages</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-info\" href=\"{{ path('transfert_new') }}\">
        <i class=\"fas fa-fw fa-share\"></i>
        <span>Transferer</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-info\" href=\"{{ path('filleuls') }}\">
        <i class=\"fas fa-fw fa-network-wired\"></i>
        <span>Mes filleuls</span>
    </a>
</li>

{#{% if app.session.get('idPayeer') == 'P1026189037' %}#}

    {#<li class=\"nav-item active\">#}
        {#<a class=\"nav-link bg-transparent\" href=\"{{ path('filleuls') }}\">#}
            {#<i class=\"fas fa-fw fa-star\"></i>#}
            {#<span>Collect de fonds pour achat crypto</span>#}
        {#</a>#}
    {#</li>#}

{#{% endif %}#}


<li class=\"nav-item active\">
    <a class=\"nav-link bg-dark\" href=\"{{ path('historiques') }}\">
        <i class=\"fas fa-fw fa-book\"></i>
        <span>Historiques</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-dark\" href=\"{{ path('faq') }}\">
        <i class=\"fas fa-fw fa-pen\"></i>
        <span>FaQs</span>
    </a>
</li>

<li class=\"nav-item active\">
    <a class=\"nav-link bg-danger\" href=\"{{ path('out') }}\">
        <i class=\"fas fa-fw fa-sign-out-alt\"></i>
        <span>Deconnexion</span>
    </a>
</li>
{#<li class=\"nav-item dropdown\">
    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"pagesDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        <i class=\"fas fa-fw fa-folder\"></i>
        <span>Pages</span>
    </a>
    <div class=\"dropdown-menu\" aria-labelledby=\"pagesDropdown\">
        <h6 class=\"dropdown-header\">Login Screens:</h6>
        <a class=\"dropdown-item\" href=\"login.html\">Login</a>
        <a class=\"dropdown-item\" href=\"register.html\">Register</a>
        <a class=\"dropdown-item\" href=\"forgot-password.html\">Forgot Password</a>
        <div class=\"dropdown-divider\"></div>
        <h6 class=\"dropdown-header\">Other Pages:</h6>
        <a class=\"dropdown-item\" href=\"404.html\">404 Page</a>
        <a class=\"dropdown-item\" href=\"blank.html\">Blank Page</a>
    </div>
</li>
<li class=\"nav-item\">
    <a class=\"nav-link\" href=\"charts.html\">
        <i class=\"fas fa-fw fa-chart-area\"></i>
        <span>Charts</span></a>
</li>
<li class=\"nav-item\">
    <a class=\"nav-link\" href=\"tables.html\">
        <i class=\"fas fa-fw fa-table\"></i>
        <span>Tables</span></a>
</li>#}

<!-- Sidebar Menu -->
{#<nav class=\"mt-2\">
    <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class=\"nav-item\">
            <a href=\"{{ path('homepage') }}\" class=\"nav-link active\">
                <i class=\"nav-icon fas fa-th\"></i>
                <p>
                    Accueil
                </p>
            </a>
        </li>

        <li class=\"nav-item\">
            <a href=\"#\" class=\"nav-link active\" style=\"background-color: green\">
                <i class=\"nav-icon fas fa-chart-area\"></i>
                <p>
                    Point
                </p>
            </a>
        </li>

        <li class=\"nav-item\">
            <a href=\"{{ path('out') }}\" class=\"nav-link active\" style=\"background-color: red\">
                <i class=\"nav-icon fas fa-sign-out-alt\"></i>
                <p>
                    Deconnexion
                </p>
            </a>
        </li>#}
        {#<li class=\"nav-item has-treeview menu-open\">#}
            {#<a href=\"#\" class=\"nav-link\">#}
                {#<i class=\"nav-icon fas fa-tachometer-alt\"></i>#}
                {#<p>#}
                    {#Dashboard#}
                    {#<i class=\"right fas fa-angle-left\"></i>#}
                {#</p>#}
            {#</a>#}
            {#<ul class=\"nav nav-treeview\">#}
                {#<li class=\"nav-item\">#}
                    {#<a href=\"./index.html\" class=\"nav-link active\">#}
                        {#<i class=\"far fa-circle nav-icon\"></i>#}
                        {#<p>Dashboard v1</p>#}
                    {#</a>#}
                {#</li>#}
                {#<li class=\"nav-item\">#}
                    {#<a href=\"./index2.html\" class=\"nav-link\">#}
                        {#<i class=\"far fa-circle nav-icon\"></i>#}
                        {#<p>Dashboard v2</p>#}
                    {#</a>#}
                {#</li>#}
                {#<li class=\"nav-item\">#}
                    {#<a href=\"./index3.html\" class=\"nav-link\">#}
                        {#<i class=\"far fa-circle nav-icon\"></i>#}
                        {#<p>Dashboard v3</p>#}
                    {#</a>#}
                {#</li>#}
            {#</ul>#}
        {#</li>#}
        {#<li class=\"nav-item\">#}
            {#<a href=\"pages/widgets.html\" class=\"nav-link\">#}
                {#<i class=\"nav-icon fas fa-th\"></i>#}
                {#<p>#}
                    {#Widgets#}
                    {#<span class=\"right badge badge-danger\">New</span>#}
                {#</p>#}
            {#</a>#}
        {#</li>#}
    {#</ul>#}
{#</nav>#}
<!-- /.sidebar-menu -->", "template/sidemenu.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\template\\sidemenu.html.twig");
    }
}
