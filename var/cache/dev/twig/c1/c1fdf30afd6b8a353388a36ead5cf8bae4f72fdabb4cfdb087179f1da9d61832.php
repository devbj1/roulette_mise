<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* visitor/indextickettirage.html.twig */
class __TwigTemplate_9152f71cf6cd272d543a7065a64458b3777b51172cf5a70b4b76726abc808c68 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return ":visitor:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/indextickettirage.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/indextickettirage.html.twig"));

        $this->parent = $this->loadTemplate(":visitor:template.html.twig", "visitor/indextickettirage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "

    <!-- Small boxes (Stat box) -->
    <div class=\"row\">
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-primary o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-comments\"></i>
                    </div>
                    <div class=\"mr-5\"> ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["montantDeposeClient"]) || array_key_exists("montantDeposeClient", $context) ? $context["montantDeposeClient"] : (function () { throw new RuntimeError('Variable "montantDeposeClient" does not exist.', 16, $this->source); })()), 0, [], "array", false, false, false, 16), "totalSum", [], "any", false, false, false, 16), "html", null, true);
        echo " USD</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage");
        echo "\">
                    <span class=\"float-left\">Total Déposer</span>
                    <span class=\"float-right\">
                  <i class=\"fas fa-angle-right\"></i>
                </span>
                </a>
            </div>
        </div>
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-info o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-user\"></i>
                    </div>
                    <div class=\"mr-5\"> ";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["montantRetireClient"]) || array_key_exists("montantRetireClient", $context) ? $context["montantRetireClient"] : (function () { throw new RuntimeError('Variable "montantRetireClient" does not exist.', 32, $this->source); })()), 0, [], "array", false, false, false, 32), "totalSum", [], "any", false, false, false, 32), "html", null, true);
        echo " USD</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage");
        echo "\">
                    <span class=\"float-left\">Total retirer</span>
                    <span class=\"float-right\">
                    <i class=\"fas fa-angle-right\"></i>
                </span>
                </a>
            </div>
        </div>
/*
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-warning o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-shopping-cart\"></i>
                    </div>
                    <div class=\"mr-5\"> 2</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Ticket</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-success o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-list\"></i>
                    </div>
                    <div class=\"mr-5\"> 3</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Ticket Gagnant</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>
*/
    </div>


    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste de mes tirages
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\">N° Tirage</th>
                        <th>Cout tirage</th>
                        <th>Ticket Acheter</th>
                        <th>Ticket Gagnant</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    ";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeTirageNumeroTirerGagnants"]) || array_key_exists("listeTirageNumeroTirerGagnants", $context) ? $context["listeTirageNumeroTirerGagnants"] : (function () { throw new RuntimeError('Variable "listeTirageNumeroTirerGagnants" does not exist.', 97, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["listeTirageNumeroTirerGagnant"]) {
            // line 98
            echo "                        <tr>
                            <td>
                                #";
            // line 100
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTirageNumeroTirerGagnant"], "id", [], "any", false, false, false, 100), "html", null, true);
            echo "
                            </td>
                            <td>";
            // line 102
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTirageNumeroTirerGagnant"], "coutTirage", [], "any", false, false, false, 102), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 104
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["listeTirageNumeroTirerGagnant"], "numeroTiragePayer", [], "any", false, false, false, 104));
            foreach ($context['_seq'] as $context["_key"] => $context["listeTicketAcheter"]) {
                // line 105
                echo "
                                    ";
                // line 106
                if ((null === twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayerStatut", [], "any", false, false, false, 106))) {
                    // line 107
                    echo "
                                        ";
                    // line 109
                    echo "                                        <a href=\"#\" class=\"btn btn-danger btn-circle leticket\"
                                           data-value=\"";
                    // line 110
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayer", [], "any", false, false, false, 110), "html", null, true);
                    echo "\">
                                            ";
                    // line 111
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayer", [], "any", false, false, false, 111), "html", null, true);
                    echo "
                                        </a>

                                    ";
                } else {
                    // line 115
                    echo "
                                        ";
                    // line 116
                    if ((twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayerStatut", [], "any", false, false, false, 116) == false)) {
                        // line 117
                        echo "
                                            <a href=\"#\" class=\"btn btn-danger btn-circle leticket\"
                                               data-value=\"";
                        // line 119
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayer", [], "any", false, false, false, 119), "html", null, true);
                        echo "\">
                                                ";
                        // line 120
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayer", [], "any", false, false, false, 120), "html", null, true);
                        echo "
                                            </a>


                                        ";
                    } else {
                        // line 125
                        echo "
                                            ";
                        // line 126
                        if ((twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayerStatut", [], "any", false, false, false, 126) == true)) {
                            // line 127
                            echo "
                                                <a href=\"#\" class=\"btn btn-success btn-circle leticket\"
                                                   data-value=\"";
                            // line 129
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayer", [], "any", false, false, false, 129), "html", null, true);
                            echo "\">
                                                    ";
                            // line 130
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketAcheter"], "numeroTicketPayer", [], "any", false, false, false, 130), "html", null, true);
                            echo "
                                                </a>

                                            ";
                        }
                        // line 134
                        echo "                                        ";
                    }
                    // line 135
                    echo "
                                    ";
                }
                // line 137
                echo "




                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listeTicketAcheter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 143
            echo "                            </td>
                            <td>

                                ";
            // line 146
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["listeTirageNumeroTirerGagnant"], "numeroTirageGagant", [], "any", false, false, false, 146));
            foreach ($context['_seq'] as $context["_key"] => $context["listeTicketGagnant"]) {
                // line 147
                echo "
                                    <a href=\"#\" class=\"btn btn-success btn-circle leticket\"
                                       data-value=\"";
                // line 149
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketGagnant"], "numeroTicketGagnant", [], "any", false, false, false, 149), "html", null, true);
                echo "\">
                                        ";
                // line 150
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTicketGagnant"], "numeroTicketGagnant", [], "any", false, false, false, 150), "html", null, true);
                echo "
                                    </a>

                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listeTicketGagnant'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 154
            echo "
                            </td>


                            <td> ";
            // line 158
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTirageNumeroTirerGagnant"], "etatTirage", [], "any", false, false, false, 158), "html", null, true);
            echo " </td>

                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listeTirageNumeroTirerGagnant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 162
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 172
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 173
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\" src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("front/tirage/js/index.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 179
        echo "    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "visitor/indextickettirage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 179,  352 => 175,  346 => 173,  336 => 172,  320 => 162,  310 => 158,  304 => 154,  294 => 150,  290 => 149,  286 => 147,  282 => 146,  277 => 143,  266 => 137,  262 => 135,  259 => 134,  252 => 130,  248 => 129,  244 => 127,  242 => 126,  239 => 125,  231 => 120,  227 => 119,  223 => 117,  221 => 116,  218 => 115,  211 => 111,  207 => 110,  204 => 109,  201 => 107,  199 => 106,  196 => 105,  192 => 104,  187 => 102,  182 => 100,  178 => 98,  174 => 97,  108 => 34,  103 => 32,  86 => 18,  81 => 16,  69 => 6,  59 => 5,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends ':visitor:template.html.twig' %}



{% block content %}


    <!-- Small boxes (Stat box) -->
    <div class=\"row\">
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-primary o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-comments\"></i>
                    </div>
                    <div class=\"mr-5\"> {{ montantDeposeClient[0].totalSum }} USD</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"{{ path('tirage') }}\">
                    <span class=\"float-left\">Total Déposer</span>
                    <span class=\"float-right\">
                  <i class=\"fas fa-angle-right\"></i>
                </span>
                </a>
            </div>
        </div>
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-info o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-user\"></i>
                    </div>
                    <div class=\"mr-5\"> {{ montantRetireClient[0].totalSum }} USD</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"{{ path('tirage') }}\">
                    <span class=\"float-left\">Total retirer</span>
                    <span class=\"float-right\">
                    <i class=\"fas fa-angle-right\"></i>
                </span>
                </a>
            </div>
        </div>
/*
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-warning o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-shopping-cart\"></i>
                    </div>
                    <div class=\"mr-5\"> 2</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Ticket</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-success o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-list\"></i>
                    </div>
                    <div class=\"mr-5\"> 3</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Ticket Gagnant</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>
*/
    </div>


    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste de mes tirages
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\">N° Tirage</th>
                        <th>Cout tirage</th>
                        <th>Ticket Acheter</th>
                        <th>Ticket Gagnant</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    {% for listeTirageNumeroTirerGagnant in listeTirageNumeroTirerGagnants %}
                        <tr>
                            <td>
                                #{{ listeTirageNumeroTirerGagnant.id }}
                            </td>
                            <td>{{ listeTirageNumeroTirerGagnant.coutTirage }}</td>
                            <td>
                                {% for listeTicketAcheter in listeTirageNumeroTirerGagnant.numeroTiragePayer %}

                                    {% if (listeTicketAcheter.numeroTicketPayerStatut is null) %}

                                        {#<a href=\"#\" class=\"btn btn-primary btn-circle leticket\"#}
                                        <a href=\"#\" class=\"btn btn-danger btn-circle leticket\"
                                           data-value=\"{{ listeTicketAcheter.numeroTicketPayer }}\">
                                            {{ listeTicketAcheter.numeroTicketPayer }}
                                        </a>

                                    {% else %}

                                        {% if (listeTicketAcheter.numeroTicketPayerStatut == false) %}

                                            <a href=\"#\" class=\"btn btn-danger btn-circle leticket\"
                                               data-value=\"{{ listeTicketAcheter.numeroTicketPayer }}\">
                                                {{ listeTicketAcheter.numeroTicketPayer }}
                                            </a>


                                        {% else %}

                                            {% if (listeTicketAcheter.numeroTicketPayerStatut == true) %}

                                                <a href=\"#\" class=\"btn btn-success btn-circle leticket\"
                                                   data-value=\"{{ listeTicketAcheter.numeroTicketPayer }}\">
                                                    {{ listeTicketAcheter.numeroTicketPayer }}
                                                </a>

                                            {% endif %}
                                        {% endif %}

                                    {% endif %}





                                {% endfor %}
                            </td>
                            <td>

                                {% for listeTicketGagnant in listeTirageNumeroTirerGagnant.numeroTirageGagant %}

                                    <a href=\"#\" class=\"btn btn-success btn-circle leticket\"
                                       data-value=\"{{ listeTicketGagnant.numeroTicketGagnant }}\">
                                        {{ listeTicketGagnant.numeroTicketGagnant }}
                                    </a>

                                {% endfor %}

                            </td>


                            <td> {{ listeTirageNumeroTirerGagnant.etatTirage }} </td>

                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

{% endblock %}



{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\" src=\"{{ asset('front/tirage/js/index.js') }}\"></script>
    {#    #}{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>#}{#
        <script src=\"{{ asset('template/js/modernizr-2.8.3.min.js') }}\"></script>
        <script src=\"{{ asset('template/chart/Chart.min.js') }}\"></script>#}
    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


{% endblock %}
", "visitor/indextickettirage.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\visitor\\indextickettirage.html.twig");
    }
}
