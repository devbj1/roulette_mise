<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* visitor/ticketgagnantnotificationpaiement.html.twig */
class __TwigTemplate_007c4e3004609e1268bdb1cee95b4bb9f6744a51d50dbf3eba5a18a59dab1b1b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return ":visitor:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/ticketgagnantnotificationpaiement.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/ticketgagnantnotificationpaiement.html.twig"));

        $this->parent = $this->loadTemplate(":visitor:template.html.twig", "visitor/ticketgagnantnotificationpaiement.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "

    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste des tirages complets
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\">N° Tirage</th>
                        <th class=\"\">Prix</th>
                        <th class=\"\">Nbr Participant</th>
                        <th class=\"\">Nbr Ticket Gagnant</th>
                        <th class=\"\">Actions</th>
                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeTicketsEnAttentePaiement"]) || array_key_exists("listeTicketsEnAttentePaiement", $context) ? $context["listeTicketsEnAttentePaiement"] : (function () { throw new RuntimeError('Variable "listeTicketsEnAttentePaiement" does not exist.', 28, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["tirages"]) {
            // line 29
            echo "                        <tr>
                            <td class=\"\">#";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "id", [], "any", false, false, false, 30), "html", null, true);
            echo "</td>
                            <td class=\"\">";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "coutTirage", [], "any", false, false, false, 31), "html", null, true);
            echo " \$</td>
                            <td class=\"\">1 - ";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "nbrNumero", [], "any", false, false, false, 32), "html", null, true);
            echo " </td>
                            <td class=\"\">";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "nbrNumeroGagnant", [], "any", false, false, false, 33), "html", null, true);
            echo " </td>
                            <td class=\"\" style=\"text-align: center\">

                                <a data-id=\"";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "id", [], "any", false, false, false, 36), "html", null, true);
            echo "\" type=\"button\"
                                   class=\"btn btn-warning confirmationpayer\"
                                   data-toggle=\"modal\"
                                   data-target=\"#modal-payer\"><i class=\"fa fa-edit\"></i>
                                </a>

                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tirages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>




    <!-- Modal -->
    <div class=\"modal fade\" id=\"modal-payer\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\"
         aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\" id=\"exampleModalLongTitle\">Payer Gagnant </h4>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"col-md-12\">
                        <div class=\"table-responsive\">
                            <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                                <thead>
                                <tr>
                                    <th class=\"\">Id Payeer</th>
                                    <th class=\"\">N° ticket gagnant</th>
                                    <th class=\"\">Gain</th>
                                    <th class=\"\" style=\"text-align: center\">Confirmer Paiement</th>
                                </tr>
                                </thead>

                                <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignegagnant\">

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class=\"modal-footer\">
                    ";
        // line 89
        echo "                </div>
            </div>
        </div>
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 99
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 100
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\" src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("front/tirage/js/index.js"), "html", null, true);
        echo "\"></script>

    ";
        // line 107
        echo "    <script>

        var listeTicketGagnantAPayerUrl = \"";
        // line 109
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajax_liste_ticket_gagnant");
        echo "\";

        var payerUrl = \"";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajax_payer_consultation");
        echo "\";

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "visitor/ticketgagnantnotificationpaiement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 111,  218 => 109,  214 => 107,  209 => 102,  203 => 100,  193 => 99,  177 => 89,  133 => 45,  118 => 36,  112 => 33,  108 => 32,  104 => 31,  100 => 30,  97 => 29,  93 => 28,  69 => 6,  59 => 5,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends ':visitor:template.html.twig' %}



{% block content %}


    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste des tirages complets
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\">N° Tirage</th>
                        <th class=\"\">Prix</th>
                        <th class=\"\">Nbr Participant</th>
                        <th class=\"\">Nbr Ticket Gagnant</th>
                        <th class=\"\">Actions</th>
                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    {% for tirages in listeTicketsEnAttentePaiement %}
                        <tr>
                            <td class=\"\">#{{ tirages.id }}</td>
                            <td class=\"\">{{ tirages.coutTirage }} \$</td>
                            <td class=\"\">1 - {{ tirages.nbrNumero}} </td>
                            <td class=\"\">{{ tirages.nbrNumeroGagnant}} </td>
                            <td class=\"\" style=\"text-align: center\">

                                <a data-id=\"{{ tirages.id }}\" type=\"button\"
                                   class=\"btn btn-warning confirmationpayer\"
                                   data-toggle=\"modal\"
                                   data-target=\"#modal-payer\"><i class=\"fa fa-edit\"></i>
                                </a>

                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>




    <!-- Modal -->
    <div class=\"modal fade\" id=\"modal-payer\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\"
         aria-hidden=\"true\">
        <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\" id=\"exampleModalLongTitle\">Payer Gagnant </h4>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"col-md-12\">
                        <div class=\"table-responsive\">
                            <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                                <thead>
                                <tr>
                                    <th class=\"\">Id Payeer</th>
                                    <th class=\"\">N° ticket gagnant</th>
                                    <th class=\"\">Gain</th>
                                    <th class=\"\" style=\"text-align: center\">Confirmer Paiement</th>
                                </tr>
                                </thead>

                                <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignegagnant\">

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class=\"modal-footer\">
                    {#<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\">Save changes</button>#}
                </div>
            </div>
        </div>
    </div>


{% endblock %}



{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\" src=\"{{ asset('front/tirage/js/index.js') }}\"></script>

    {#    #}{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>#}{#
        <script src=\"{{ asset('template/js/modernizr-2.8.3.min.js') }}\"></script>
        <script src=\"{{ asset('template/chart/Chart.min.js') }}\"></script>#}
    <script>

        var listeTicketGagnantAPayerUrl = \"{{ path('ajax_liste_ticket_gagnant') }}\";

        var payerUrl = \"{{ path('ajax_payer_consultation') }}\";

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


{% endblock %}
", "visitor/ticketgagnantnotificationpaiement.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\visitor\\ticketgagnantnotificationpaiement.html.twig");
    }
}
