<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login.html.twig */
class __TwigTemplate_8b107de7e1bb08f7a413f47fc44b9b40ff41fecd2c459ec6077067d20ac3fdbf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>Roulette | Se Connecter</title>

    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
    <!--===============================================================================================-->
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/images/icons/icon3.png"), "html", null, true);
        echo "\"/>
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/css/util.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/css/main.css"), "html", null, true);
        echo "\">
    <!--===============================================================================================-->


</head>

<body onload=\"getTime()\">
<!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->

<div class=\"limiter\">
    <div class=\"container-login100\">
        <div class=\"wrap-login100\">
            <div class=\"login100-form-title\"
                 style=\"background-image: url(";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/images/bg-01.jpg"), "html", null, true);
        echo ");\">
\t\t\t\t\t<span class=\"login100-form-title-1\">
\t\t\t\t\t\tConnectez-vous !
\t\t\t\t\t</span>
            </div>

            <form class=\"login100-form validate-form\" action=\"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
        echo "\" method=\"post\">
                <div class=\"wrap-input100 validate-input m-b-26\" data-validate=\"Identifiant Payeer Requis\">
                    ";
        // line 38
        echo "                    <span class=\"label-input100\"><img src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/images/icons/payeer.png"), "html", null, true);
        echo "\"
                                                      width=\"35\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"\"
                                                      data-original-title=\"\"></span>
                    <input class=\"input100\" type=\"text\" required name=\"idpayeer\" placeholder=\"Enter l'identifiant payeer\">
                    <span class=\"focus-input100\"></span>
                </div>
                <div class=\"wrap-input100 validate-input m-b-26\" data-validate=\"Pin\">
                    ";
        // line 46
        echo "
                    <span class=\"label-input100\">Password :</span>
                    <input class=\"input100\" required type=\"password\" name=\"pin\" placeholder=\"Enter votre password | Définissez en un\">
                    <span class=\"focus-input100\"></span>

                </div>
                <div class=\"flex-sb-m w-full p-b-30\">
                    <div class=\"contact100-form-checkbox\">

                        <div class=\"centered\" style=\"text-align: center\"><b style=\"color: red\">";
        // line 55
        echo twig_escape_filter($this->env, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 55, $this->source); })()), "html", null, true);
        echo "</b></div>
                    </div>

                    <div>
                        <hr>
                        <div id=\"showtime\" style=\"color: blue\"></div>
                    </div>
                </div>

                <div class=\"container-login100-form-btn\">
                    <button class=\"login100-form-btn\">
                        Debuter
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<script src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/vendor/jquery/jquery-3.2.1.min.js"), "html", null, true);
        echo "\"></script>

<!--===============================================================================================-->
<script src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/js/main.js"), "html", null, true);
        echo "\"></script>
";
        // line 81
        echo "
";
        // line 87
        echo "<script>
    function getTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('showtime').innerHTML = h + \":\" + m + \":\" + s;
        t = setTimeout(function () {
            getTime()
        }, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = \"0\" + i;
        }
        return i;
    }
</script>
</body>

</html>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 87,  151 => 81,  147 => 78,  141 => 75,  118 => 55,  107 => 46,  96 => 38,  91 => 35,  82 => 29,  64 => 14,  60 => 13,  55 => 11,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>Roulette | Se Connecter</title>

    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
    <!--===============================================================================================-->
    <link rel=\"icon\" type=\"image/png\" href=\"{{ asset('template/login/images/icons/icon3.png') }}\"/>
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('template/login/css/util.css') }}\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('template/login/css/main.css') }}\">
    <!--===============================================================================================-->


</head>

<body onload=\"getTime()\">
<!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->

<div class=\"limiter\">
    <div class=\"container-login100\">
        <div class=\"wrap-login100\">
            <div class=\"login100-form-title\"
                 style=\"background-image: url({{ asset('template/login/images/bg-01.jpg') }});\">
\t\t\t\t\t<span class=\"login100-form-title-1\">
\t\t\t\t\t\tConnectez-vous !
\t\t\t\t\t</span>
            </div>

            <form class=\"login100-form validate-form\" action=\"{{ path('login') }}\" method=\"post\">
                <div class=\"wrap-input100 validate-input m-b-26\" data-validate=\"Identifiant Payeer Requis\">
                    {#<span class=\"label-input100\">Identifiant Payeer</span>#}
                    <span class=\"label-input100\"><img src=\"{{ asset('template/login/images/icons/payeer.png') }}\"
                                                      width=\"35\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"\"
                                                      data-original-title=\"\"></span>
                    <input class=\"input100\" type=\"text\" required name=\"idpayeer\" placeholder=\"Enter l'identifiant payeer\">
                    <span class=\"focus-input100\"></span>
                </div>
                <div class=\"wrap-input100 validate-input m-b-26\" data-validate=\"Pin\">
                    {#<span class=\"label-input100\">Identifiant Payeer</span>#}

                    <span class=\"label-input100\">Password :</span>
                    <input class=\"input100\" required type=\"password\" name=\"pin\" placeholder=\"Enter votre password | Définissez en un\">
                    <span class=\"focus-input100\"></span>

                </div>
                <div class=\"flex-sb-m w-full p-b-30\">
                    <div class=\"contact100-form-checkbox\">

                        <div class=\"centered\" style=\"text-align: center\"><b style=\"color: red\">{{ error }}</b></div>
                    </div>

                    <div>
                        <hr>
                        <div id=\"showtime\" style=\"color: blue\"></div>
                    </div>
                </div>

                <div class=\"container-login100-form-btn\">
                    <button class=\"login100-form-btn\">
                        Debuter
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<script src=\"{{ asset('template/login/vendor/jquery/jquery-3.2.1.min.js') }}\"></script>

<!--===============================================================================================-->
<script src=\"{{ asset('template/login/js/main.js') }}\"></script>
{#<!--BACKSTRETCH-->#}
{#<script src=\"{{ asset('template/login/js/jquery.backstretch.min.js') }}\"></script>#}

{#<script>#}
{#\$.backstretch(\"../template/\", {#}
{#speed: 500#}
{#});#}
{#</script>#}
<script>
    function getTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('showtime').innerHTML = h + \":\" + m + \":\" + s;
        t = setTimeout(function () {
            getTime()
        }, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = \"0\" + i;
        }
        return i;
    }
</script>
</body>

</html>

", "security/login.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\security\\login.html.twig");
    }
}
