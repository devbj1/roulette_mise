<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* visitor/tirage.html.twig */
class __TwigTemplate_c7aa55094d9949727576dddcb5bf3cc56bd8a3e62836948da21128028b6c6a82 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return ":visitor:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/tirage.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/tirage.html.twig"));

        $this->parent = $this->loadTemplate(":visitor:template.html.twig", "visitor/tirage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "

    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste des tirages en cours
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\" hidden>id</th>
                        <th class=\"\">N° Tirage</th>
                        <th class=\"\">Prix</th>
                        <th class=\"\">Nbr Participant</th>
                        <th class=\"\">Nbr Ticket Gagnant</th>
                        <th class=\"\">Statut</th>
                        <th class=\"\">Actions</th>
                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeTirages"]) || array_key_exists("listeTirages", $context) ? $context["listeTirages"] : (function () { throw new RuntimeError('Variable "listeTirages" does not exist.', 29, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["tirages"]) {
            // line 30
            echo "                        <tr>
                            <td class=\"\" hidden>";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "id", [], "any", false, false, false, 31), "html", null, true);
            echo "</td>
                            <td class=\"\">#";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "id", [], "any", false, false, false, 32), "html", null, true);
            echo "</td>
                            <td class=\"\">";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "coutTirage", [], "any", false, false, false, 33), "html", null, true);
            echo " \$</td>
                            <td class=\"\">1 - ";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "nbrNumero", [], "any", false, false, false, 34), "html", null, true);
            echo " </td>
                            <td class=\"\">";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "nbrNumeroGagnant", [], "any", false, false, false, 35), "html", null, true);
            echo " </td>
                            <td class=\"\">";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "etatTirage", [], "any", false, false, false, 36), "html", null, true);
            echo " </td>
                            <td class=\"\" style=\"text-align: center\">
                                <a type=\"button\" class=\"btn btn-primary\"
                                   href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("souscrire_tirage", ["id" => twig_get_attribute($this->env, $this->source, $context["tirages"], "id", [], "any", false, false, false, 39)]), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tirages"], "coutTirage", [], "any", false, false, false, 39), "html", null, true);
            echo " \$ x2 <i
                                            class=\"fa fa-edit\"></i></a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tirages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "                    </tbody>
                </table>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 54
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 55
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("front/tirage/js/index.js"), "html", null, true);
        echo "\"></script>

    ";
        // line 62
        echo "    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "visitor/tirage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 62,  173 => 57,  167 => 55,  157 => 54,  141 => 44,  128 => 39,  122 => 36,  118 => 35,  114 => 34,  110 => 33,  106 => 32,  102 => 31,  99 => 30,  95 => 29,  69 => 5,  59 => 4,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends ':visitor:template.html.twig' %}


{% block content %}


    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Liste des tirages en cours
        </div>
        <div class=\"card-body\">
            <div class=\"table-responsive\">
                <table class=\"table table-bordered\" id=\"tablelistetirage\" width=\"100%\" cellspacing=\"0\">
                    <thead>
                    <tr>
                        <th class=\"\" hidden>id</th>
                        <th class=\"\">N° Tirage</th>
                        <th class=\"\">Prix</th>
                        <th class=\"\">Nbr Participant</th>
                        <th class=\"\">Nbr Ticket Gagnant</th>
                        <th class=\"\">Statut</th>
                        <th class=\"\">Actions</th>
                    </tr>
                    </thead>

                    <tbody role=\"alert\" aria-live=\"polite\" aria-relevant=\"all\" id=\"tablelignetirage\">
                    {% for tirages in listeTirages %}
                        <tr>
                            <td class=\"\" hidden>{{ tirages.id }}</td>
                            <td class=\"\">#{{ tirages.id }}</td>
                            <td class=\"\">{{ tirages.coutTirage }} \$</td>
                            <td class=\"\">1 - {{ tirages.nbrNumero}} </td>
                            <td class=\"\">{{ tirages.nbrNumeroGagnant}} </td>
                            <td class=\"\">{{ tirages.etatTirage}} </td>
                            <td class=\"\" style=\"text-align: center\">
                                <a type=\"button\" class=\"btn btn-primary\"
                                   href=\"{{ path('souscrire_tirage', { 'id': tirages.id }) }}\"> {{ tirages.coutTirage }} \$ x2 <i
                                            class=\"fa fa-edit\"></i></a>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

{% endblock %}



{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\" src=\"{{ asset('front/tirage/js/index.js') }}\"></script>

    {#    #}{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>#}{#
        <script src=\"{{ asset('template/js/modernizr-2.8.3.min.js') }}\"></script>
        <script src=\"{{ asset('template/chart/Chart.min.js') }}\"></script>#}
    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


{% endblock %}
", "visitor/tirage.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\visitor\\tirage.html.twig");
    }
}
