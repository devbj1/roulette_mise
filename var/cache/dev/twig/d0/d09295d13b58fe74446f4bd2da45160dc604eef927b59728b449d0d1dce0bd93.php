<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* visitor/payerticket.html.twig */
class __TwigTemplate_3c7433f1013b0ebc5f273b5b3e663cdcb566e62e9a48a22faf33da31db4a78f7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return ":visitor:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/payerticket.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/payerticket.html.twig"));

        $this->parent = $this->loadTemplate(":visitor:template.html.twig", "visitor/payerticket.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "


    <ol class=\"breadcrumb\">
        <li class=\"breadcrumb-item\">
            <h4 id=\"ntirage\" data-value=\"";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["tirage"]) || array_key_exists("tirage", $context) ? $context["tirage"] : (function () { throw new RuntimeError('Variable "tirage" does not exist.', 10, $this->source); })()), "id", [], "any", false, false, false, 10), "html", null, true);
        echo "\">Tirage N° ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["tirage"]) || array_key_exists("tirage", $context) ? $context["tirage"] : (function () { throw new RuntimeError('Variable "tirage" does not exist.', 10, $this->source); })()), "id", [], "any", false, false, false, 10), "html", null, true);
        echo ", coût ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["tirage"]) || array_key_exists("tirage", $context) ? $context["tirage"] : (function () { throw new RuntimeError('Variable "tirage" does not exist.', 10, $this->source); })()), "coutTirage", [], "any", false, false, false, 10), "html", null, true);
        echo " \$
                de ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["tirage"]) || array_key_exists("tirage", $context) ? $context["tirage"] : (function () { throw new RuntimeError('Variable "tirage" does not exist.', 11, $this->source); })()), "nbrNumero", [], "any", false, false, false, 11), "html", null, true);
        echo " ticket dont ";
        echo twig_escape_filter($this->env, (isset($context["nbrTicketDispo"]) || array_key_exists("nbrTicketDispo", $context) ? $context["nbrTicketDispo"] : (function () { throw new RuntimeError('Variable "nbrTicketDispo" does not exist.', 11, $this->source); })()), "html", null, true);
        echo " disponible || Sélectionner le ticket et
                cliquer sur payer pour l'acheter</h4>
        </li>

    </ol>

    <ol class=\"breadcrumb\" style=\"background-color: #0b3e6f\">
        <div class=\"card-body\">
            <!-- Circle Buttons (Default) -->
            ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listeTicketsTirage"]) || array_key_exists("listeTicketsTirage", $context) ? $context["listeTicketsTirage"] : (function () { throw new RuntimeError('Variable "listeTicketsTirage" does not exist.', 20, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["ticket"]) {
            // line 21
            echo "
                ";
            // line 22
            if ((twig_get_attribute($this->env, $this->source, $context["ticket"], "ticketAcheter", [], "any", false, false, false, 22) == false)) {
                // line 23
                echo "                    <a href=\"#\" class=\"btn btn-primary btn-circle leticket\" data-value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ticket"], "numeroTicket", [], "any", false, false, false, 23), "html", null, true);
                echo "\">
                        ";
                // line 24
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ticket"], "numeroTicket", [], "any", false, false, false, 24), "html", null, true);
                echo "
                    </a>


                ";
            } else {
                // line 29
                echo "                    <a href=\"#\" class=\"btn btn-danger btn-circle leticket\" data-value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ticket"], "numeroTicket", [], "any", false, false, false, 29), "html", null, true);
                echo "\">
                        ";
                // line 30
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ticket"], "numeroTicket", [], "any", false, false, false, 30), "html", null, true);
                echo "
                    </a>

                ";
            }
            // line 34
            echo "


            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ticket'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "

        </div>
    </ol>


    ";
        // line 45
        echo "        ";
        // line 46
        echo "        ";
        // line 47
        echo "        ";
        // line 48
        echo "        ";
        // line 49
        echo "        ";
        // line 50
        echo "        ";
        // line 51
        echo "        ";
        // line 52
        echo "        ";
        // line 53
        echo "        ";
        // line 54
        echo "        ";
        // line 55
        echo "        ";
        // line 56
        echo "    ";
        // line 57
        echo "
    <a href=\"\" class=\"btn btn-success btn-icon-split\" id=\"btn_paiement_solde\">
                    <span class=\"icon text-white-50\">
                      <i class=\"fas fa-arrow-right\"></i>
                    </span>
        <span class=\"text\">Payer Via Mon Solde</span>
    </a>


    <a href=\"\" class=\"btn btn-success btn-icon-split\" id=\"btn_paiement_payeer\" disabled>
                    <span class=\"icon text-white-50\">
                      <i class=\"fas fa-arrow-right\"></i>
                    </span>
        <span class=\"text\" disabled>Payer Via Payeer</span>
    </a>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 77
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 78
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("front/tirage/js/index.js"), "html", null, true);
        echo "\"></script>

    ";
        // line 84
        echo "    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "visitor/payerticket.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 84,  214 => 79,  209 => 78,  199 => 77,  173 => 57,  171 => 56,  169 => 55,  167 => 54,  165 => 53,  163 => 52,  161 => 51,  159 => 50,  157 => 49,  155 => 48,  153 => 47,  151 => 46,  149 => 45,  141 => 38,  132 => 34,  125 => 30,  120 => 29,  112 => 24,  107 => 23,  105 => 22,  102 => 21,  98 => 20,  84 => 11,  76 => 10,  69 => 5,  59 => 4,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends ':visitor:template.html.twig' %}


{% block content %}



    <ol class=\"breadcrumb\">
        <li class=\"breadcrumb-item\">
            <h4 id=\"ntirage\" data-value=\"{{ tirage.id }}\">Tirage N° {{ tirage.id }}, coût {{ tirage.coutTirage }} \$
                de {{ tirage.nbrNumero }} ticket dont {{ nbrTicketDispo }} disponible || Sélectionner le ticket et
                cliquer sur payer pour l'acheter</h4>
        </li>

    </ol>

    <ol class=\"breadcrumb\" style=\"background-color: #0b3e6f\">
        <div class=\"card-body\">
            <!-- Circle Buttons (Default) -->
            {% for ticket in listeTicketsTirage %}

                {% if (ticket.ticketAcheter == false) %}
                    <a href=\"#\" class=\"btn btn-primary btn-circle leticket\" data-value=\"{{ ticket.numeroTicket }}\">
                        {{ ticket.numeroTicket }}
                    </a>


                {% else %}
                    <a href=\"#\" class=\"btn btn-danger btn-circle leticket\" data-value=\"{{ ticket.numeroTicket }}\">
                        {{ ticket.numeroTicket }}
                    </a>

                {% endif %}



            {% endfor %}


        </div>
    </ol>


    {#<form method=\"post\" action=\"https://payeer.com/merchant/\">#}
        {#<input type=\"hidden\" name=\"m_shop\" value=\"1046859706\">#}
        {#<input type=\"hidden\" name=\"m_orderid\" value=\"1\">#}
        {#<input type=\"hidden\" name=\"m_amount\" value=\"1\">#}
        {#<input type=\"hidden\" name=\"m_curr\" value=\"USD\">#}
        {#<input type=\"hidden\" name=\"m_desc\" value=\"<? = \$ m_desc?>\">#}
        {#<input type=\"hidden\" name=\"m_sign\" value=\"<? = \$ sign?>\">#}
        {#<input type = \"hidden\" name = \"form [ps]\" value = \"2609\">#}
        {#<input type=\"hidden\" name=\"form [curr [2609]]\" value=\"USD\">#}
        {#<input type = \"hidden\" name = \"m_params\" value = \"<? = \$ m_params?>\">#}
        {#<input type=\"hidden\" name=\"m_cipher_method\" value=\"AES-256-CBC\">#}
        {#<input type=\"submit\" name=\"m_process\" value=\"send\"/>#}
    {#</form>#}

    <a href=\"\" class=\"btn btn-success btn-icon-split\" id=\"btn_paiement_solde\">
                    <span class=\"icon text-white-50\">
                      <i class=\"fas fa-arrow-right\"></i>
                    </span>
        <span class=\"text\">Payer Via Mon Solde</span>
    </a>


    <a href=\"\" class=\"btn btn-success btn-icon-split\" id=\"btn_paiement_payeer\" disabled>
                    <span class=\"icon text-white-50\">
                      <i class=\"fas fa-arrow-right\"></i>
                    </span>
        <span class=\"text\" disabled>Payer Via Payeer</span>
    </a>

{% endblock %}



{% block javascripts %}
    {{ parent() }}
    <script type=\"text/javascript\" src=\"{{ asset('front/tirage/js/index.js') }}\"></script>

    {#    #}{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>#}{#
        <script src=\"{{ asset('template/js/modernizr-2.8.3.min.js') }}\"></script>
        <script src=\"{{ asset('template/chart/Chart.min.js') }}\"></script>#}
    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


{% endblock %}
", "visitor/payerticket.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\visitor\\payerticket.html.twig");
    }
}
