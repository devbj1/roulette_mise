<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* visitor/index.html.twig */
class __TwigTemplate_e76e7beb410a4370d339fd45e067ca395e4d285ecd6101d468461d1bcf1b1475 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return ":visitor:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "visitor/index.html.twig"));

        $this->parent = $this->loadTemplate(":visitor:template.html.twig", "visitor/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 8
        echo "

    <!-- Small boxes (Stat box) -->
    <div class=\"row\">
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-primary o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-comments\"></i>
                    </div>
                    <div class=\"mr-5\">Paiement automatique après 24h suite au tirage</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage");
        echo "\">
                    <span class=\"float-left\">Montant Minimum Ticket : 1 USD</span>
                    <span class=\"float-right\">
                  <i class=\"fas fa-angle-right\"></i>
                </span>
                </a>
            </div>
        </div>
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-info o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-user\"></i>
                    </div>
                    <div class=\"mr-5\">";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["nbrClient"]) || array_key_exists("nbrClient", $context) ? $context["nbrClient"] : (function () { throw new RuntimeError('Variable "nbrClient" does not exist.', 34, $this->source); })()), "html", null, true);
        echo "</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Investisseurs</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-warning o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-shopping-cart\"></i>
                    </div>
                    <div class=\"mr-5\"> ";
        // line 50
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["montantDepose"]) || array_key_exists("montantDepose", $context) ? $context["montantDepose"] : (function () { throw new RuntimeError('Variable "montantDepose" does not exist.', 50, $this->source); })()), 0, [], "array", false, false, false, 50), "totalSum", [], "any", false, false, false, 50), 2, ",", "."), "html", null, true);
        echo "  \$</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Dollars Déposer</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-success o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-list\"></i>
                    </div>
                    <div class=\"mr-5\">";
        // line 66
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["montantRetire"]) || array_key_exists("montantRetire", $context) ? $context["montantRetire"] : (function () { throw new RuntimeError('Variable "montantRetire" does not exist.', 66, $this->source); })()), 0, [], "array", false, false, false, 66), "totalSum", [], "any", false, false, false, 66), 2, ",", "."), "html", null, true);
        echo "  \$</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Dollars Payer</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

    </div>

    <br>

    <div class=\"row\" id=\"payment-proofs\">
        <div class=\"col-12\">
            <div class=\"card\">
                <div class=\"card-block p-b-0\">
                    <h4 class=\"card-title\">Dernieres Operations</h4>
                    <br>
                </div>

                <ul class=\"nav nav-tabs customtab\" role=\"tablist\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#tabRecentDeposits\" role=\"tab\">Recent Achat
                            de ticket</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tabLastPayouts\" role=\"tab\">Derniers Paiement</a>
                    </li>

                </ul>

                <div class=\"tab-content\">
                    <div class=\"tab-pane p-20 active\" id=\"tabRecentDeposits\" role=\"tabpanel\">
                        <div class=\"table-responsive\">
                            <table class=\"table table-striped\">
                                <tbody>
                                <tr>
                                </tr>
                                </tbody>
                                <thead>
                                <tr>
                                    <th>Date Heure</th>
                                    <th>Participant Id</th>
                                    <th>Montant</th>

                                </tr>
                                </thead>
                                <tbody>


                                ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listesTop5MontantDepose"]) || array_key_exists("listesTop5MontantDepose", $context) ? $context["listesTop5MontantDepose"] : (function () { throw new RuntimeError('Variable "listesTop5MontantDepose" does not exist.', 118, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["listeTop5MontantDepose"]) {
            // line 119
            echo "                                    <tr>
                                        <td>";
            // line 120
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTop5MontantDepose"], "dateOperation", [], "any", false, false, false, 120), "m/d/Y h:m:s"), "html", null, true);
            echo "</td>
                                        <td><img src=\"";
            // line 121
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/images/icons/payeer.png"), "html", null, true);
            echo "\" width=\"17\"
                                                 data-toggle=\"tooltip\" data-placement=\"left\" title=\"\"
                                                 data-original-title=\"\">&nbsp;&nbsp;&nbsp;
                                            <span>

                            \t\t\t\t\t\t\t<a href=\"#\">";
            // line 126
            echo twig_escape_filter($this->env, twig_slice($this->env, strip_tags(twig_get_attribute($this->env, $this->source, $context["listeTop5MontantDepose"], "idPayeer", [], "any", false, false, false, 126)), 0, 5), "html", null, true);
            echo "....</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span></td>
                                        <td>";
            // line 128
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTop5MontantDepose"], "montantOperation", [], "any", false, false, false, 128), 2, ",", "."), "html", null, true);
            echo "  \$</td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listeTop5MontantDepose'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 131
        echo "

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class=\"tab-pane p-20\" id=\"tabLastPayouts\" role=\"tabpanel\">
                        <div class=\"table-responsive\">
                            <table class=\"table table-striped\">
                                <thead>
                                <tr>
                                    <th>Date Heure</th>
                                    <th>Participant Id</th>
                                    <th>Montant</th>


                                </tr>
                                </thead>
                                <tbody>
                                ";
        // line 150
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listesTop5MontantRetire"]) || array_key_exists("listesTop5MontantRetire", $context) ? $context["listesTop5MontantRetire"] : (function () { throw new RuntimeError('Variable "listesTop5MontantRetire" does not exist.', 150, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["listeTop5MontantRetire"]) {
            // line 151
            echo "                                    <tr>
                                        <td>";
            // line 152
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTop5MontantRetire"], "dateOperation", [], "any", false, false, false, 152), "m/d/Y h:m:s"), "html", null, true);
            echo "</td>
                                        <td><img src=\"";
            // line 153
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("template/login/images/icons/payeer.png"), "html", null, true);
            echo "\" width=\"17\"
                                                 data-toggle=\"tooltip\" data-placement=\"left\" title=\"\"
                                                 data-original-title=\"\">&nbsp;&nbsp;&nbsp;
                                            <span>

                            \t\t\t\t\t\t\t<a href=\"#\">";
            // line 158
            echo twig_escape_filter($this->env, twig_slice($this->env, strip_tags(twig_get_attribute($this->env, $this->source, $context["listeTop5MontantRetire"], "idPayeer", [], "any", false, false, false, 158)), 0, 5), "html", null, true);
            echo "....</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span></td>
                                        <td>";
            // line 160
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["listeTop5MontantRetire"], "montantOperation", [], "any", false, false, false, 160), 2, ",", "."), "html", null, true);
            echo "  \$</td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listeTop5MontantRetire'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 163
        echo "
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    ";
        // line 175
        echo "
    ";
        // line 177
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 183
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 184
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


    ";
        // line 190
        echo "    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "visitor/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  325 => 190,  318 => 184,  308 => 183,  296 => 177,  293 => 175,  280 => 163,  271 => 160,  266 => 158,  258 => 153,  254 => 152,  251 => 151,  247 => 150,  226 => 131,  217 => 128,  212 => 126,  204 => 121,  200 => 120,  197 => 119,  193 => 118,  138 => 66,  119 => 50,  100 => 34,  83 => 20,  69 => 8,  59 => 7,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends ':visitor:template.html.twig' %}





{% block content %}


    <!-- Small boxes (Stat box) -->
    <div class=\"row\">
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-primary o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-comments\"></i>
                    </div>
                    <div class=\"mr-5\">Paiement automatique après 24h suite au tirage</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"{{ path('tirage') }}\">
                    <span class=\"float-left\">Montant Minimum Ticket : 1 USD</span>
                    <span class=\"float-right\">
                  <i class=\"fas fa-angle-right\"></i>
                </span>
                </a>
            </div>
        </div>
        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-info o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-user\"></i>
                    </div>
                    <div class=\"mr-5\">{{ nbrClient }}</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Investisseurs</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-warning o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-shopping-cart\"></i>
                    </div>
                    <div class=\"mr-5\"> {{ montantDepose[0].totalSum|number_format(2, ',', '.') }}  \$</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Dollars Déposer</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

        <div class=\"col-xl-3 col-sm-6 mb-3\">
            <div class=\"card text-white bg-success o-hidden h-100\">
                <div class=\"card-body\">
                    <div class=\"card-body-icon\">
                        <i class=\"fas fa-fw fa-list\"></i>
                    </div>
                    <div class=\"mr-5\">{{ montantRetire[0].totalSum|number_format(2, ',', '.') }}  \$</div>
                </div>
                <a class=\"card-footer text-white clearfix small z-1\" href=\"\">
                    <span class=\"float-left\">Total Dollars Payer</span>
                    <span class=\"float-right\">
                </span>
                </a>
            </div>
        </div>

    </div>

    <br>

    <div class=\"row\" id=\"payment-proofs\">
        <div class=\"col-12\">
            <div class=\"card\">
                <div class=\"card-block p-b-0\">
                    <h4 class=\"card-title\">Dernieres Operations</h4>
                    <br>
                </div>

                <ul class=\"nav nav-tabs customtab\" role=\"tablist\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#tabRecentDeposits\" role=\"tab\">Recent Achat
                            de ticket</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tabLastPayouts\" role=\"tab\">Derniers Paiement</a>
                    </li>

                </ul>

                <div class=\"tab-content\">
                    <div class=\"tab-pane p-20 active\" id=\"tabRecentDeposits\" role=\"tabpanel\">
                        <div class=\"table-responsive\">
                            <table class=\"table table-striped\">
                                <tbody>
                                <tr>
                                </tr>
                                </tbody>
                                <thead>
                                <tr>
                                    <th>Date Heure</th>
                                    <th>Participant Id</th>
                                    <th>Montant</th>

                                </tr>
                                </thead>
                                <tbody>


                                {% for listeTop5MontantDepose in listesTop5MontantDepose %}
                                    <tr>
                                        <td>{{ listeTop5MontantDepose.dateOperation|date(\"m/d/Y h:m:s\") }}</td>
                                        <td><img src=\"{{ asset('template/login/images/icons/payeer.png') }}\" width=\"17\"
                                                 data-toggle=\"tooltip\" data-placement=\"left\" title=\"\"
                                                 data-original-title=\"\">&nbsp;&nbsp;&nbsp;
                                            <span>

                            \t\t\t\t\t\t\t<a href=\"#\">{{ listeTop5MontantDepose.idPayeer | striptags | slice(0, 5) }}....</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span></td>
                                        <td>{{ listeTop5MontantDepose.montantOperation|number_format(2, ',', '.') }}  \$</td>
                                    </tr>
                                {% endfor %}


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class=\"tab-pane p-20\" id=\"tabLastPayouts\" role=\"tabpanel\">
                        <div class=\"table-responsive\">
                            <table class=\"table table-striped\">
                                <thead>
                                <tr>
                                    <th>Date Heure</th>
                                    <th>Participant Id</th>
                                    <th>Montant</th>


                                </tr>
                                </thead>
                                <tbody>
                                {% for listeTop5MontantRetire in listesTop5MontantRetire %}
                                    <tr>
                                        <td>{{ listeTop5MontantRetire.dateOperation|date(\"m/d/Y h:m:s\") }}</td>
                                        <td><img src=\"{{ asset('template/login/images/icons/payeer.png') }}\" width=\"17\"
                                                 data-toggle=\"tooltip\" data-placement=\"left\" title=\"\"
                                                 data-original-title=\"\">&nbsp;&nbsp;&nbsp;
                                            <span>

                            \t\t\t\t\t\t\t<a href=\"#\">{{ listeTop5MontantRetire.idPayeer | striptags | slice(0, 5) }}....</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span></td>
                                        <td>{{ listeTop5MontantRetire.montantOperation|number_format(2, ',', '.') }}  \$</td>
                                    </tr>
                                {% endfor %}

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {#<div id=\"contenu\">#}

    {#</div>#}


{% endblock %}



{% block javascripts %}
    {{ parent() }}


    {#    #}{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>#}{#
        <script src=\"{{ asset('template/js/modernizr-2.8.3.min.js') }}\"></script>
        <script src=\"{{ asset('template/chart/Chart.min.js') }}\"></script>#}
    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


{% endblock %}
", "visitor/index.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\visitor\\index.html.twig");
    }
}
