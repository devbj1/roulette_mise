<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* tirage/new.html.twig */
class __TwigTemplate_ac632ecb7257603db208c505fd71cdb2ed511183a7170b82d3f76dbce701cc2e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return ":tirage:template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tirage/new.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tirage/new.html.twig"));

        $this->parent = $this->loadTemplate(":tirage:template.html.twig", "tirage/new.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "

    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Ajout de tirage
        </div>
        ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), 'form_start');
        echo "
        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), "coutTirage", [], "any", false, false, false, 18), 'row', ["attr" => ["class" => "form-control"]]);
        echo "
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "nbrNumero", [], "any", false, false, false, 23), 'row', ["attr" => ["class" => "form-control"]]);
        echo "
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                <input class=\"btn btn-success\" type=\"submit\" value=\"Create\" />
                <a type=\"button\" class=\"form-group btn btn-danger\" href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("tirage_index");
        echo "\">Retour a la liste</a>
            </div>
        </div>


        ";
        // line 34
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 34, $this->source); })()), 'form_end');
        echo "


    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 43
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 44
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("front/tirage/js/index.js"), "html", null, true);
        echo "\"></script>

    ";
        // line 51
        echo "    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "tirage/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 51,  142 => 46,  136 => 44,  126 => 43,  110 => 34,  102 => 29,  93 => 23,  85 => 18,  79 => 15,  69 => 7,  59 => 6,  36 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("
{% extends ':tirage:template.html.twig' %}



{% block content %}


    <!-- DataTables Example -->
    <div class=\"card mb-3\">
        <div class=\"card-header\">
            <i class=\"fas fa-table\"></i>
            Ajout de tirage
        </div>
        {{ form_start(form) }}
        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                {{ form_row(form.coutTirage ,{'attr':{'class':'form-control'}}) }}
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                {{ form_row(form.nbrNumero ,{'attr':{'class':'form-control'}}) }}
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-6\">
                <input class=\"btn btn-success\" type=\"submit\" value=\"Create\" />
                <a type=\"button\" class=\"form-group btn btn-danger\" href=\"{{ path('tirage_index') }}\">Retour a la liste</a>
            </div>
        </div>


        {{ form_end(form) }}


    </div>

{% endblock %}



{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\" src=\"{{ asset('front/tirage/js/index.js') }}\"></script>

    {#    #}{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>#}{#
        <script src=\"{{ asset('template/js/modernizr-2.8.3.min.js') }}\"></script>
        <script src=\"{{ asset('template/chart/Chart.min.js') }}\"></script>#}
    <script>

        // \$(\".accueil\").addClass('active');


        // \$(window).load(function () {
        //
        //
        //     // Animate loader off screen
        //     /*  setTimeout(function () {
        //           \$(\".se-pre-con\").fadeOut(\"slow\");
        //       }, 1000);*/
        //     //\$(\".se-pre-con\").fadeOut(\"2000\");
        // });


    </script>


{% endblock %}


", "tirage/new.html.twig", "C:\\laragon\\www\\roulette_mise\\app\\Resources\\views\\tirage\\new.html.twig");
    }
}
