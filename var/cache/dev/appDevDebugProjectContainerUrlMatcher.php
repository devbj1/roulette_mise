<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // ajax_liste_ticket_gagnant
        if ('/listeticketgagnant' === $pathinfo) {
            return array (  '_controller' => 'AjaxBundle\\Controller\\DefaultController::listTicketGagnantAction',  '_route' => 'ajax_liste_ticket_gagnant',);
        }

        // ajax_payer_consultation
        if ('/payerticketgagnant' === $pathinfo) {
            return array (  '_controller' => 'AjaxBundle\\Controller\\DefaultController::payerTicketGagnantAction',  '_route' => 'ajax_payer_consultation',);
        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        // faq
        if ('/faq' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::faqAction',  '_route' => 'faq',);
        }

        // filleuls
        if ('/filleuls' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::filleulsAction',  '_route' => 'filleuls',);
        }

        // historiques
        if ('/historiques' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::historiquesAction',  '_route' => 'historiques',);
        }

        // notifier_gagnant
        if ('/notifiergagnant' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::notifierGagnantAction',  '_route' => 'notifier_gagnant',);
        }

        // tirage
        if ('/tirage' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::tirageAction',  '_route' => 'tirage',);
        }

        // souscrire_tirage
        if (preg_match('#^/(?P<id>[^/]++)/tirage$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'souscrire_tirage']), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::souscrireTirageAction',));
        }

        // transfert_new
        if ('/transfert' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::transfertAction',  '_route' => 'transfert_new',);
        }

        // recharge_new
        if ('/recharge' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::rechargeAction',  '_route' => 'recharge_new',);
        }

        // paiement_solde
        if (preg_match('#^/(?P<idtirage>[^/]++)/(?P<idticket>[^/]++)/paiementsolde$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'paiement_solde']), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::paiementSoldeAction',));
        }

        // paiement_payeer
        if (preg_match('#^/(?P<idtirage>[^/]++)/(?P<idticket>[^/]++)/paiementpayeer$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'paiement_payeer']), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::paiementPayeerAction',));
        }

        // membre_order_new
        if (0 === strpos($pathinfo, '/members/order') && preg_match('#^/members/order(?:/(?P<action>[^/]++)(?:/(?P<status>[^/]++)(?:/(?P<item_number>[^/]++))?)?)?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'membre_order_new']), array (  'action' => '',  'status' => '',  'item_number' => '',  '_controller' => 'AppBundle\\Controller\\DefaultController::statutPayeerAction',));
        }

        // login
        if (0 === strpos($pathinfo, '/login') && preg_match('#^/login(?:/(?P<idparrain>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'login']), array (  'idparrain' => NULL,  '_controller' => 'AppBundle\\Controller\\SecurityController::loginAction',));
        }

        // out
        if ('/out' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::outAction',  '_route' => 'out',);
        }

        if (0 === strpos($pathinfo, '/tirage')) {
            // tirage_index
            if ('/tirage' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'AppBundle\\Controller\\TirageController::indexAction',  '_route' => 'tirage_index',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_tirage_index;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'tirage_index'));
                }

                return $ret;
            }
            not_tirage_index:

            // tirage_my_index
            if ('/tirage/mestirages' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\TirageController::indexMyAction',  '_route' => 'tirage_my_index',);
            }

            // tirage_new
            if ('/tirage/new' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\TirageController::newAction',  '_route' => 'tirage_new',);
            }

            // tirage_show
            if (preg_match('#^/tirage/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'tirage_show']), array (  '_controller' => 'AppBundle\\Controller\\TirageController::showAction',));
            }

            // tirage_edit
            if (preg_match('#^/tirage/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'tirage_edit']), array (  '_controller' => 'AppBundle\\Controller\\TirageController::editAction',));
            }

            // tirage_delete
            if (preg_match('#^/tirage/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'tirage_delete']), array (  '_controller' => 'AppBundle\\Controller\\TirageController::deleteAction',));
            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
