<?php

    namespace AppBundle\Controller;

    use AppBundle\Entity\Client;
    use AppBundle\Entity\ClientTicket;
    use AppBundle\Entity\Tirage;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Session\Session;

    /**
     * Tirage controller.
     *
     * @Route("tirage")
     */
    class TirageController extends Controller
    {
        /**
         * Lists all tirage entities.
         *
         * @Route("/", name="tirage_index")
         * @Method("GET")
         */
        public function indexAction()
        {
            $this->userConnect();

            $em = $this->getDoctrine()->getManager();

            $tirages = $em->getRepository('AppBundle:Tirage')->findAll();

            return $this->render('tirage/index.html.twig', array(
                'tirages' => $tirages,
            ));
        }

        /**
         * Lists all tirage entities.
         *
         * @Route("/mestirages", name="tirage_my_index")
         * @Method("GET")
         */
        public function indexMyAction()
        {

            $this->userConnect();


            $session = new Session();

            $em = $this->getDoctrine()->getManager();

            $tirages = $em->getRepository('AppBundle:Tirage')->findAllTirageOfClient($session->get('codeClient'));

            $listeTirageNumeroTirerGagnants = [];
            foreach ($tirages as $tirage) {
                $ticketTirage = new \stdClass();
                $ticketTirage->id = $tirage->getId();
                $ticketTirage->etatTirage = $tirage->getEtatTirage();
                $ticketTirage->coutTirage = $tirage->getCoutTirage();

                $ticketsAcheter = $em->getRepository('AppBundle:Tirage')->findNumeroPayerTirageOfClient($tirage->getId(), $session->get('codeClient'));
                //dump($ticketsAcheter);die();
                $listeTicketAcheterTirage = [];
                foreach ($ticketsAcheter as $ticketAcheter) {
                    $ticketTiragePayer = new \stdClass();
                   // dump($ticketsAcheter[0]['numeroTicket']);die();
                    $ticketTiragePayer->numeroTicketPayer = $ticketAcheter['numeroTicket'];
                    $ticketTiragePayer->numeroTicketPayerStatut = $ticketAcheter['ticketGagnant'];
                    $listeTicketAcheterTirage[] = $ticketTiragePayer;
                }
                $ticketTirage->numeroTiragePayer = $listeTicketAcheterTirage;


                $ticketsGagnant = $em->getRepository('AppBundle:Tirage')->findNumeroGagnantTirage($tirage->getId());
                //dump($ticketsAcheter);die();
                $listeTicketGagnantTirage = [];
                foreach ($ticketsGagnant as $ticketGagnant) {
                    $ticketTirageGagnant = new \stdClass();
                   // dump($ticketsAcheter[0]['numeroTicket']);die();
                    $ticketTirageGagnant->numeroTicketGagnant = $ticketGagnant['numeroTicket'];
                    $listeTicketGagnantTirage[] = $ticketTirageGagnant;
                }

                $ticketTirage->numeroTirageGagant = $listeTicketGagnantTirage;

                $listeTirageNumeroTirerGagnants[] = $ticketTirage;
            }

            //dump($listeTirageNumeroTirerGagnants);

            //dump($listeTirageNumeroTirerGagnants);die();

            $montantDeposeClient = $em->getRepository('AppBundle:Operation')->sumTypeOperationClient('Entrée',$session->get('codeClient'));

            //dump($listeTirageNumeroTirerGagnants);die();
            $montantRetireClient = $em->getRepository('AppBundle:Operation')->sumTypeOperationClient('Sortie',$session->get('codeClient'));

            //dump($montantDeposeClient);die();
            //die();
            return $this->render('visitor/indextickettirage.html.twig',
                compact('listeTirageNumeroTirerGagnants','montantDeposeClient','montantRetireClient'));
        }

        /**
         * Creates a new tirage entity.
         *
         * @Route("/new", name="tirage_new")
         * @Method({"GET", "POST"})
         */
        public function newAction(Request $request)
        {
            $this->userConnect();

            $tirage = new Tirage();
            $form = $this->createForm('AppBundle\Form\TirageType', $tirage);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $tirage->setNbrNumeroGagnant($tirage->getNbrNumero()*0.4);
                $em->persist($tirage);
                $em->flush();

                $client = $em->getRepository('AppBundle:Client')->findOneBy(array("id" => 1));

                //Creer les tickets
                for ($i = 1; $i <= $tirage->getNbrNumero(); $i++) {

                    $clientTicket = new ClientTicket();
                    $clientTicket->setClient($client);
                    $clientTicket->setTirage($tirage);
                    $clientTicket->setNumeroTicket($i);
                    $em->persist($clientTicket);
                    $em->flush();

                };

                $session = new Session();
                if ($tirage = 0) {
                    $session->getFlashBag()->add('error', 'Erreur enregistrement !');
                } else {
                    $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
                }
                return $this->redirectToRoute('tirage_index');
            }

            return $this->render('tirage/new.html.twig', array(
                'tirage' => $tirage,
                'form' => $form->createView(),
            ));
        }

        /**
         * Finds and displays a tirage entity.
         *
         * @Route("/{id}", name="tirage_show")
         * @Method("GET")
         */
        public function showAction(Tirage $tirage)
        {
            $this->userConnect();

            $deleteForm = $this->createDeleteForm($tirage);

            return $this->render('tirage/show.html.twig', array(
                'tirage' => $tirage,
                'delete_form' => $deleteForm->createView(),
            ));
        }

        /**
         * Displays a form to edit an existing tirage entity.
         *
         * @Route("/{id}/edit", name="tirage_edit")
         * @Method({"GET", "POST"})
         */
        public function editAction(Request $request, Tirage $tirage)
        {
            $this->userConnect();

            $deleteForm = $this->createDeleteForm($tirage);
            $editForm = $this->createForm('AppBundle\Form\TirageType', $tirage);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('tirage_edit', array('id' => $tirage->getId()));
            }

            return $this->render('tirage/edit.html.twig', array(
                'tirage' => $tirage,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }

        /**
         * Deletes a tirage entity.
         *
         * @Route("/{id}", name="tirage_delete")
         * @Method("DELETE")
         */
        public function deleteAction(Request $request, Tirage $tirage)
        {
            $this->userConnect();

            $form = $this->createDeleteForm($tirage);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($tirage);
                $em->flush();
            }

            return $this->redirectToRoute('tirage_index');
        }

        /**
         * Creates a form to delete a tirage entity.
         *
         * @param Tirage $tirage The tirage entity
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createDeleteForm(Tirage $tirage)
        {
            return $this->createFormBuilder()
                ->setAction($this->generateUrl('tirage_delete', array('id' => $tirage->getId())))
                ->setMethod('DELETE')
                ->getForm();
        }



        public function userConnect()
        {
            $session = new Session();
            $connect = $session->get('authenticated');
            if ($connect != true) {
                $url = $this->generateUrl('login');
                $response = new RedirectResponse($url);
                $response->send();
                return;
            }else{
                $em = $this->getDoctrine()->getManager();

                $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                $session->set("soldeClient", $client->getSolde());

            }
        }
    }
