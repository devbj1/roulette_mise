<?php

    namespace AppBundle\Controller;


    use AppBundle\Entity\Client;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


    class SecurityController extends Controller
    {

        /**
         * @Route("/login/{idparrain}", name="login")
         * @Method({"GET", "POST"})
         */
        public function loginAction(Request $request, $idparrain = null)
        {

            $session = new Session();

            
//            dump($idparrain);die();
            if ($request->getMethod() == 'GET') {

                $session->set("lienparrain", $idparrain);
                
                return $this->render('security/login.html.twig', [
                    'error' => "",
                ]);
            }

            if ($request->getMethod() == 'POST') {
                $idpayeer = $request->get("idpayeer");
                $session = new Session();
                //dump($this->getParameter('accountnumber'), $this->getParameter('apiId'), $this->getParameter('apiKey'));die();

                $payeer = new \CPayeer($this->getParameter('accountNumber'), $this->getParameter('apiId'), $this->getParameter('apiKey'));

                //dump($payeer->getErrors());die();
                if ($payeer->isAuth()) {

                    if ($payeer->checkUser(array(
                        'user' => $request->get("idpayeer"),
                    ))) {

                    } else {

                        return $this->render('security/login.html.twig', [
                            'error' => "Identifiant incorrecte",
                        ]);
                    }

                } else {
                    //dump($payeer->getErrors());die();
                    return $this->render('security/login.html.twig', [
                        'error' => "Vérifier votre connexion internet !",
//                        'error' => "Erreur système! Vérifier votre connexion internet !",
                        //'error' => "Erreur système! Vérifier votre connexion internet ! Si cela persiste veuillez contacter l'administrateur !",
                    ]);

                }

//                if (strlen($idpayeer) != 11) {
//                    $session->set("authenticated", false);
//                    return $this->render('security/login.html.twig', [
//                        'error' => "Identifiant incorrecte",
//                    ]);
//                }

                $em = $this->getDoctrine()->getManager();
                // get the login error if there is one
                $identifyValide = $em->getRepository("AppBundle:Client")->findBy(array('idPayeer' => $idpayeer));
                if ($identifyValide != []) {

                    //VerifiePin
                    if ($identifyValide[0]->getPin() != $request->get("pin")) {
                        return $this->render('security/login.html.twig', [
                            'error' => "Password incorrecte",
                        ]);
                    }
                    $session->set("authenticated", true);
                    $session->set("codeClient", $identifyValide[0]->getId());
                    $session->set("idPayeer", $identifyValide[0]->getIdPayeer());
                    $session->set("soldeClient", $identifyValide[0]->getSolde());
                    $session->set("lienParrainage", $identifyValide[0]->getIdParrainage());


                } else {

//                    $parrain = new Client();

//                    dump($session->get("idpayeer"));die();
                    if ($session->get("lienparrain") == null) {
                        $parrain = $em->getRepository('AppBundle:Client')->find(1);
                    } else {
                        $parrain = $em->getRepository('AppBundle:Client')->findBy(array('idParrainage' => $session->get("lienparrain")));
                        $parrain = $em->getRepository('AppBundle:Client')->find($parrain[0]->getId());
                    }


                    $idParrainage = $this->genererChaineAleatoire();
                    $client = new Client();
                    $client->setIdPayeer($idpayeer);
                    $client->setParrain($parrain);
                    $client->setIdParrainage($idParrainage);
                    $client->setPin($request->get("pin"));
                    $em->persist($client);
                    $em->flush();


                    $session->set("authenticated", true);
                    $session->set("codeClient", $client->getId());
                    $session->set("soldeClient", $client->getSolde());
                    $session->set("lienParrainage", $client->getIdParrainage());
                    $session->set("idPayeer", $idpayeer);

                }
                return $this->redirectToRoute('homepage');

            }
        }

//
//        /**
//         * @Route("/login/{idparrain}", name="login_parrainage")
//         * @Method({"GET", "POST"})
//         */
//        public function loginParrainageAction(Request $request, $idparrain)
//        {
//
//            if ($request->getMethod() == 'GET') {
//                return $this->render('security/login.html.twig', [
//                    'error' => "",
//                ]);
//            }
//
//            if ($request->getMethod() == 'POST') {
//                $idpayeer = $request->get("idpayeer");
//                $session = new Session();
//                //dump($this->getParameter('accountnumber'), $this->getParameter('apiId'), $this->getParameter('apiKey'));die();
//
//                $payeer = new \CPayeer($this->getParameter('accountnumber'), $this->getParameter('apiId'), $this->getParameter('apiKey'));
//
//                dump("ok");
//                die();
//
//                if ($payeer->isAuth()) {
//                    if ($payeer->checkUser(array(
//                        'user' => $request->get("idPayeer"),
//                    ))) {
//
//                    } else {
//
//                        return $this->render('security/login.html.twig', [
//                            'error' => "Identifiant incorrecte",
//                        ]);
//                    }
//                } else {
//                    //dump($payeer->getErrors());die();
//                    return $this->render('security/login.html.twig', [
//                        'error' => "Erreur système ! Veuillez contacter l'administrateur !",
//                    ]);
//
//                }
//
////                if (strlen($idpayeer) != 11) {
////                    $session->set("authenticated", false);
////                    return $this->render('security/login.html.twig', [
////                        'error' => "Identifiant incorrecte",
////                    ]);
////                }
//
//                $em = $this->getDoctrine()->getManager();
//                // get the login error if there is one
//                $identifyValide = $em->getRepository("AppBundle:Client")->findBy(array('idPayeer' => $idpayeer));
//                if ($identifyValide != []) {
//
//                    //VerifiePin
//                    if ($identifyValide[0]->getPin() != $request->get("pin")) {
//                        return $this->render('security/login.html.twig', [
//                            'error' => "Password incorrecte",
//                        ]);
//                    }
//                    $session->set("authenticated", true);
//                    $session->set("codeClient", $identifyValide[0]->getId());
//                    $session->set("idPayeer", $identifyValide[0]->getIdPayeer());
//                    $session->set("soldeClient", $identifyValide[0]->getSolde());
//                    $session->set("lienParrainage", $identifyValide[0]->getIdParrainage());
//
//                } else {
//
////                    $parrain = new Client();
//
//                    //if ($idparrain == null){
//                    //$parrain = $em->getRepository('AppBundle:Client')->find(1);
//                    //}else{
//                    $parrain = $em->getRepository('AppBundle:Client')->findBy(array('idParrainage' => $idparrain));
//
//                    //}
//
//                    dump($parrain);
//                    die();
//                    $idParrainage = $this->genererChaineAleatoire();
//                    $client = new Client();
//                    $client->setIdPayeer($idpayeer);
//                    $client->setParrain($parrain);
//                    $client->setIdParrainage($idParrainage);
//                    $client->setPin($request->get("pin"));
//                    $em->persist($client);
//                    $em->flush();
//
//
//                    $session->set("authenticated", true);
//                    $session->set("codeClient", $client->getId());
//                    $session->set("soldeClient", $client->getSolde());
//                    $session->set("lienParrainage", $client->getIdParrainage());
//                    $session->set("idPayeer", $idpayeer);
//
//
//                }
//                return $this->redirectToRoute('homepage');
//
//            }
//        }

        public function userConnect()
        {
            $session = new Session();
            $connect = $session->get('authenticated');
            if ($connect != true) {
                $url = $this->generateUrl('login');
                $response = new RedirectResponse($url);
                $response->send();
                return;
            } else {
                $em = $this->getDoctrine()->getManager();

                $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                $session->set("soldeClient", $client->getSolde());

            }
        }


        function genererChaineAleatoire($longueur = 10)
        {
            $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $longueurMax = strlen($caracteres);
            $chaineAleatoire = '';
            for ($i = 0; $i < $longueur; $i++) {
                $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
            }
            return $chaineAleatoire;
        }


        /**
         * @Route("/out", name="out")
         * @Method({"GET", "POST"})
         */
        public function outAction()
        {
            //dump("Ok");die();
            $session = new Session();
            $session->set("authenticated", false);
            $session->set("idPayeer", "");
            $session->set("codeClient", "");
            $session->set("soldeClient", "");
            return $this->render('security/login.html.twig', [
                'error' => "",
            ]);

        }


    }