<?php

    namespace AppBundle\Controller;

    use AppBundle\Entity\Operation;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Session\Session;
    use yii;
    use yii\base;
    use yiidreamteam\payeer\RedirectForm;


    class DefaultController extends Controller
    {
        /**
         * @Route("/", name="homepage")
         */
        public function indexAction()
        {
            // replace this example code with whatever you need
            $this->userConnect();

            $em = $this->getDoctrine()->getManager();

            $nbrClient = count($em->getRepository('AppBundle:Client')->findAll());


            $montantDepose = $em->getRepository('AppBundle:Operation')->sumTypeOperation('Entrée');

            $listesTop5MontantDepose = $em->getRepository('AppBundle:Operation')->findTypeOperation('Entrée');


            $montantRetire = $em->getRepository('AppBundle:Operation')->sumTypeOperation('Sortie');

            $listesTop5MontantRetire = $em->getRepository('AppBundle:Operation')->findTypeOperation('Sortie');

            //dump($listeTop5MontantDepose);die();

            return $this->render('visitor/index.html.twig', compact('nbrClient', 'montantDepose', 'montantRetire', 'listesTop5MontantDepose', 'listesTop5MontantRetire'));

        }

        /**
         * @Route("/faq", name="faq")
         */
        public function faqAction()
        {
            // replace this example code with whatever you need
            $this->userConnect();

            //$em = $this->getDoctrine()->getManager();

            //$listeTirages = $em->getRepository('AppBundle:Tirage')->findAll();

            return $this->render('visitor/faq.html.twig');

        }


        /**
         * @Route("/filleuls", name="filleuls")
         */
        public function filleulsAction()
        {
            // replace this example code with whatever you need
            $this->userConnect();
            $session = new Session();
            $em = $this->getDoctrine()->getManager();

            //dump("ok");die();
            $listeFilleuls = $em->getRepository('AppBundle:Client')->findBy(array('parrain' => $session->get('codeClient')));

            return $this->render('visitor/filleuls.html.twig', compact('listeFilleuls'));

        }


        /**
         * @Route("/historiques", name="historiques")
         */
        public function historiquesAction()
        {
            // replace this example code with whatever you need
            $this->userConnect();
            $session = new Session();
            $em = $this->getDoctrine()->getManager();

            //dump("ok");die();
            $listeHistoriques = $em->getRepository('AppBundle:Operation')->findOperationClient($session->get('codeClient'));

            return $this->render('visitor/historiques.html.twig', compact('listeHistoriques'));

        }


        /**
         * @Route("/notifiergagnant", name="notifier_gagnant")
         */
        public function notifierGagnantAction()
        {
            // replace this example code with whatever you need
            $this->userConnect();
            //$session = new Session();
            $em = $this->getDoctrine()->getManager();

            $listeTirages = $em->getRepository('AppBundle:Tirage')->findBy(array('etatTirage' => 'Terminé'));
//            dump($listeTirages);die();

            $listeTicketsEnAttentePaiement = [];
            foreach ($listeTirages as $listeTirage) {
                $listeTicketTirageNonPayer = $em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $listeTirage, 'estPayer' => null, 'ticketGagnant' => true));

//                dump($listeTicketTirageNonPayer);
                if ($listeTicketTirageNonPayer != null) {
                    $listeConsul = [];
                    //dump($listeTirage);die();
                    $listeConsul['id'] = $listeTirage->getId();
                    $listeConsul['coutTirage'] = $listeTirage->getCoutTirage();
                    $listeConsul['nbrNumero'] = $listeTirage->getNbrNumero();
                    $listeConsul['nbrNumeroGagnant'] = $listeTirage->getNbrNumeroGagnant();

                    array_push($listeTicketsEnAttentePaiement, $listeConsul);

                }
            }
//            dump($listeTicketsEnAttentePaiement);die();

            return $this->render('visitor/ticketgagnantnotificationpaiement.html.twig', compact('listeTicketsEnAttentePaiement'));

        }


        /**
         * @Route("/tirage", name="tirage")
         */
        public function tirageAction()
        {
            // replace this example code with whatever you need
            $this->userConnect();

            $em = $this->getDoctrine()->getManager();

            $listeTirages = $em->getRepository('AppBundle:Tirage')->findAllTirageOfNoEqualStatut('Terminé');

            return $this->render('visitor/tirage.html.twig', compact('listeTirages'));

        }


        /**
         * @Route("/{id}/tirage", name="souscrire_tirage")
         * @Method({"GET", "POST"})
         */
        public function souscrireTirageAction($id)
        {
            // replace this example code with whatever you need
            $this->userConnect();

            $em = $this->getDoctrine()->getManager();

            //$tirage = new Tirage();

            $tirage = $em->getRepository('AppBundle:Tirage')->find($id);

            $nbrTicketDispo = $tirage->getNbrNumero() - count($em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $tirage->getId(), 'ticketAcheter' => true)));

            $listeTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findAllNumberOfTicket($tirage);


            //dump($listeTicketsTirage);die();

            return $this->render('visitor/payerticket.html.twig', compact('tirage', 'nbrTicketDispo', 'listeTicketsTirage'));

        }


        /**
         * Transfert solde.
         *
         * @Route("/transfert", name="transfert_new")
         * @Method({"GET", "POST"})
         */
        public function transfertAction(Request $request)
        {
            $this->userConnect();


            if ($request->getMethod() == "POST") {

                $session = new Session();
                $numeroPayeerBeneficiaire = $request->get('idpayeerbeneficiaire');
                $montantTransfert = floatval($request->get('montanttransfert'));
                //dump($montantTransfert);


                if ($numeroPayeerBeneficiaire == '') {
                    $session->getFlashBag()->add('error', 'Veuillez saisir le numéro du payeer bénéficiaire');
                    return $this->render('visitor/transfert.html.twig');

                }
                if ($montantTransfert == 0) {
                    $session->getFlashBag()->add('error', 'Veuillez saisir le montant');
                    return $this->render('visitor/transfert.html.twig');

                }

                //dump($montantTransfert);die();

                $em = $this->getDoctrine()->getManager();

                $idPayeer = $session->get("idPayeer");
                $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                $clientReception = $em->getRepository('AppBundle:Client')->findBy(array("idPayeer" => $numeroPayeerBeneficiaire));

                //dump($client->getId());die();
                if ($client->getId() != null) {
                    if ($clientReception != null) {
                        if ($client->getPin() == $request->get('pin')) {
                            $solde = floatval($client->getSolde());
                            $montantTransfertTTC = floatval($montantTransfert * 1.01);
                            if ($montantTransfertTTC > $solde) {
                                $session->getFlashBag()->add('error', 'Votre solde ne couvre pas cette transaction');
                                return $this->render('visitor/transfert.html.twig');

                            } else {


                                //dump(intval($solde),intval($montantTransfertTTC));die();
                                $client->setSolde(floatval($solde - $montantTransfertTTC));
                                $em->persist($client);
                                $em->flush();

                                $operation = new Operation();
                                $operation->setMotifOperation('Transfert de ' . $idPayeer . ' a ' . $numeroPayeerBeneficiaire . ' de ' . $montantTransfert . '$');
                                $operation->setHistoriquePaiement("Transfert");
                                $operation->setMontantOperation(floatval($montantTransfertTTC));
                                $operation->setClient($client);
                                //$operation->setTirage($tirage);
                                $operation->setTypeOperation('Transfert');
                                $em->persist($operation);
                                $em->flush();

                                $session->set("soldeClient", $client->getSolde());

                                $operation = new Operation();

                                //dump($numeroPayeerBeneficiaire,$clientReception);die();
                                $client = $em->getRepository('AppBundle:Client')->find($clientReception[0]->getId());

                                $client->setSolde(floatval($client->getSolde() + $montantTransfert));
                                $em->persist($client);
                                $em->flush();

                                $operation->setMotifOperation('Reception de transfert de ' . $numeroPayeerBeneficiaire . ' a ' . $idPayeer . ' de ' . $montantTransfert . '$');
                                $operation->setHistoriquePaiement("Transfert");
                                $operation->setMontantOperation(floatval($montantTransfertTTC));
                                $operation->setClient($client);
                                //$operation->setTirage($tirage);
                                $operation->setTypeOperation('Transfert');
                                $em->persist($operation);
                                $em->flush();


                                $session->getFlashBag()->add('success', 'Transaction effectuée avec succès !');

                            }

                        } else {

                            $session->getFlashBag()->add('error', 'Password Incorrect !');

                        }

                    } else {
                        $session->getFlashBag()->add('error', 'Compte invalide !');

                    }


                }

            }


            return $this->render('visitor/transfert.html.twig');
        }


        /**
         * Transfert solde.
         *
         * @Route("/recharge", name="recharge_new")
         * @Method({"GET", "POST"})
         */
        public function rechargeAction(Request $request)
        {
            $this->userConnect();
            $session = new Session();

            if ($request->getMethod() == "POST") {

                $montantRecharge = floatval($request->get('montantrecharge'));
//                dump($montantRecharge);die();

                if ($montantRecharge == 0) {
                    $session->getFlashBag()->add('error', 'Veuillez saisir le montant');
                    return $this->render('visitor/transfert.html.twig');

                }
                $em = $this->getDoctrine()->getManager();
                $idPayeer = $request->get("idPayeer");
                $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                //dump($client->getId());die();
                if ($client->getId() != null) {

//                    dump($request->get('pin'));die();
                    if ($client->getPin() == $request->get('pin')) {
                        $solde = floatval($client->getSolde());

                        $operation = new Operation();

                        $client->setSolde(floatval($solde + $montantRecharge));
                        $em->persist($client);
                        $em->flush();

                        $session->set("soldeClient", $client->getSolde());


                        $operation->setMotifOperation('Rechargement ' . $idPayeer . ' de ' . $montantRecharge . '$');
                        $operation->setHistoriquePaiement("Recharge");
                        $operation->setMontantOperation(floatval($montantRecharge));
                        $operation->setClient($client);
                        //$operation->setTirage($tirage);
                        $operation->setTypeOperation('Recharge');
                        $em->persist($operation);
                        $em->flush();

                        $session->getFlashBag()->add('success', 'Recharge effectuée avec succès !');


                    } else {
                        $session->getFlashBag()->add('error', 'Password Incorrect !');
                    }

                } else {
                    $session->getFlashBag()->add('error', 'Compte invalide !');

                }
            }

            return $this->render('visitor/recharge.html.twig');
        }


        /**
         * @Route("/{idtirage}/{idticket}/paiementsolde", name="paiement_solde")
         * @Method({"GET", "POST"})
         */
        public function paiementSoldeAction($idtirage, $idticket)
        {
            $this->userConnect();

            $session = new Session();

            $em = $this->getDoctrine()->getManager();

            $tirage = $em->getRepository('AppBundle:Tirage')->find($idtirage);

            $nbrTicketDispo = $tirage->getNbrNumero() - count($em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $tirage->getId(), 'ticketAcheter' => true)));

            $nbrTicket = $tirage->getNbrNumero();

            $nbrTicketGagnant = $tirage->getNbrNumeroGagnant();

            $listeTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findStatutNumberOfTicket($idticket, $tirage);


            //$listesTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findAllNumberOfTicket($tirage);


            //die();

            //dump("ok");die();


            if ($listeTicketsTirage[0]->isTicketAcheter() === null) {

                $prixTicket = $listeTicketsTirage[0]->getTirage()->getCoutTirage();


                $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                //$tirage = $em->getRepository('AppBundle:Ticket')->find($session->get('codeClient'));
                $solde = floatval($client->getSolde());
                if ($prixTicket > $solde) {
                    $session->getFlashBag()->add('error', 'Votre solde ne couvre pas cette transaction');
                    //  return $this->render('visitor/transfert.html.twig');

                } else {

                    $operation = new Operation();
                    $operation->setMotifOperation('Achat ticket N° ' . $idticket . ' pour le tirage ' . $idtirage . ' au coût de ' . $prixTicket);
                    $operation->setHistoriquePaiement("Achat du solde");
                    $operation->setMontantOperation($prixTicket);
                    $operation->setClient($client);
                    $operation->setTirage($tirage);
                    $operation->setTypeOperation('Entrée');
                    $em->persist($operation);
                    $em->flush();


                    $clientTicket = $em->getRepository('AppBundle:ClientTicket')->find($listeTicketsTirage[0]->getId());
                    $clientTicket->setTicketAcheter(true);
                    $clientTicket->setClient($client);
                    $clientTicket->setDateAchatTicket(new \DateTime('now'));
                    $em->persist($clientTicket);
                    $em->flush();

                    $client->setSolde(floatval($client->getSolde()) - floatval($prixTicket));
                    $em->persist($client);
                    $em->flush();

                    $session->set("soldeClient", $client->getSolde());

                    $nbrTicketRestant = count($em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $tirage, 'ticketAcheter' => null)));

                    //dump($nbrTicketRestant);die();

                    if ($nbrTicketRestant == 0) {
                        //dump($nbrTicketRestant);die();
                        $tirage->setEtatTirage('Dépouillement en cours');

                        $listeTicket = [];
                        //$listeTicketGagant = [];

                        for ($i = 1; $i <= $nbrTicket; $i++) {
                            array_push($listeTicket, $i);
                        }


                        $listeTicketGagant = array_rand($listeTicket, $nbrTicketGagnant);

                        for ($j = 0; $j <= $nbrTicketGagnant - 1; $j++) {

                            $detailsTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findStatutNumberOfTicket($listeTicket[$listeTicketGagant[$j]], $tirage);

                            $ticketTirage = $em->getRepository('AppBundle:ClientTicket')->find($detailsTicketsTirage[0]->getId());

                            $ticketTirage->setTicketGagnant(1);

                            $em->persist($ticketTirage);

                            $em->flush();

                        }

                        $tirage->setEtatTirage('Terminé');

                    } else {
                        $tirage->setEtatTirage('Plus que ' . $nbrTicketRestant . ' disponible');
                    }


                    $em->persist($tirage);
                    $em->flush();

                    $session->getFlashBag()->add('success', 'Ticket achetez avec success !');

                }


            } else {
                $session->getFlashBag()->add('error', 'Ticket déjà acheter par un autre membre');
            }

            return $this->render('visitor/payerticket.html.twig', compact('tirage', 'nbrTicketDispo', 'listeTicketsTirage'));

        }


        /**
         * @Route("/{idtirage}/{idticket}/paiementpayeer", name="paiement_payeer")
         * @Method({"GET", "POST"})
         */
        public function paiementPayeerAction($idtirage, $idticket)
        {
            $this->userConnect();

            $session = new Session();

            $session->set('idTiragePayeer', $idtirage);
            $session->set('idTicketPayeer', $idticket);

            $em = $this->getDoctrine()->getManager();

            $tirage = $em->getRepository('AppBundle:Tirage')->find($idtirage);

            $nbrTicketDispo = $tirage->getNbrNumero() - count($em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $tirage->getId(), 'ticketAcheter' => true)));

            $listeTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findStatutNumberOfTicket($idticket, $tirage);


//            $nbrTicket = $tirage->getNbrNumero();
//
//            $nbrTicketGagnant = $tirage->getNbrNumeroGagnant();

            if ($listeTicketsTirage[0]->isTicketAcheter() === null) {

                $prixTicket = $listeTicketsTirage[0]->getTirage()->getCoutTirage();

                $payeer = new \CPayeer($this->getParameter('accountNumber'), $this->getParameter('apiId'), $this->getParameter('apiKey'));

                //dump($payeer->getErrors());die();
                if ($payeer->isAuth()) {
                    //Donnée du ticket


                $m_shop = '1059359404';
                $m_orderid = '1';
                $m_amount = number_format(0.01, 2, '.', '');
                $m_curr = 'USD';
                $m_desc = base64_encode('Test');
                $m_key = '123';

                $arHash = array(
                   $m_shop,
                   $m_orderid,
                   $m_amount,
                   $m_curr,
                   $m_desc
                );

                $arHash[] = $m_key;

                $sign = strtoupper(hash('sha256', implode(':', $arHash)));

                $arGetParams = array(
                    'm_shop' => $m_shop,
                    'm_orderid' => $m_orderid,
                    'm_amount' => $m_amount,
                    'm_curr' => $m_curr,
                    'm_desc' => $m_desc,
                    'm_sign' => $sign,
                    //'m_params' => $params,
                    //'m_cipher_method' => 'AES-256-CBC',
                    //'form[ps]' => '2609',
                    //'form[curr[2609]]' => 'USD',
                );

                $url = 'https://payeer.com/merchant/?'.http_build_query($arGetParams);

                    return new RedirectResponse($url);

//                    dump($arShop);
//                    dump($payeer->getErrors());die();
/*                    if ($historyId > 0) {

                        $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                        //$tirage = $em->getRepository('AppBundle:Ticket')->find($session->get('codeClient'));


                        $operation = new Operation();
                        $operation->setMotifOperation('Achat ticket N° ' . $idticket . ' pour le tirage ' . $idtirage . ' a ' . $prixTicket);
                        $operation->setHistoriquePaiement($historyId);
                        $operation->setMontantOperation($prixTicket);
                        $operation->setClient($client);
                        $operation->setTirage($tirage);
                        $operation->setTypeOperation('Entrée');
                        $em->persist($operation);
                        $em->flush();


                        $clientTicket = $em->getRepository('AppBundle:ClientTicket')->find($listeTicketsTirage[0]->getId());
                        $clientTicket->setTicketAcheter(true);
                        $clientTicket->setClient($client);
                        $clientTicket->setDateAchatTicket(new \DateTime('now'));

                        $em->persist($clientTicket);
                        $em->flush();

                        $nbrTicketRestant = count($em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $tirage, 'ticketAcheter' => false)));
                        if ($nbrTicketRestant == 0) {
                            //dump($nbrTicketRestant);die();
                            $tirage->setEtatTirage('Dépouillement en cours');

                            $listeTicket = [];
                            //$listeTicketGagant = [];

                            for ($i = 1; $i <= $nbrTicket; $i++) {
                                array_push($listeTicket, $i);
                            }


                            $listeTicketGagant = array_rand($listeTicket, $nbrTicketGagnant);

                            for ($j = 0; $j <= $nbrTicketGagnant - 1; $j++) {

                                $detailsTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findStatutNumberOfTicket($listeTicket[$listeTicketGagant[$j]], $tirage);

                                $ticketTirage = $em->getRepository('AppBundle:ClientTicket')->find($detailsTicketsTirage[0]->getId());

                                $ticketTirage->setTicketGagnant(1);

                                $em->persist($ticketTirage);

                                $em->flush();

                            }

                            $tirage->setEtatTirage('Terminé');

                        } else {
                            $tirage->setEtatTirage('Plus que ' . $nbrTicketRestant . ' disponible');
                        }


                        $em->persist($tirage);
                        $em->flush();


                        $session->getFlashBag()->add('success', 'Ticket achetez avec success !');
                    } else {

                        $session->getFlashBag()->add('error', 'Erreur lors du paiement !');
                        //dump($payeer->getErrors());
                        //die();
                    }*/


                    /*                    } else {

                                            $session->getFlashBag()->add('error', 'Transaction annulée !');
                                            dump($payeer->getErrors());
                                            die();
                                        }*/
                } else {


                    $session->getFlashBag()->add('error', 'Erreur système ! Veuillez contacter l\'administrateur !');
                    //dump($payeer->getErrors());die();
                }

            } else {
                $session->getFlashBag()->add('error', 'Ticket déjà acheter par un autre membre');
            }

            return $this->render('visitor/payerticket.html.twig', compact('tirage', 'nbrTicketDispo', 'listeTicketsTirage'));

        }


        /**
         * Payeer Statut Operation.
         *
         * @Route("/members/order/{action}/{status}/{item_number}", name="membre_order_new")
         * @Method("GET")
         */
        public function statutPayeerAction(Request $request, $action = "", $status="", $item_number="")
        {
            $this->userConnect();
            $session = new Session();

                    $em = $this->getDoctrine()->getManager();
                    $idtirage = $session->get('idTiragePayeer');
                    $idticket = $session->get('idTicketPayeer');
                    $tirage = $em->getRepository('AppBundle:Tirage')->find($idtirage);

                    $nbrTicketDispo = $tirage->getNbrNumero() - count($em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $tirage->getId(), 'ticketAcheter' => true)));

                    $listeTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findStatutNumberOfTicket($idticket, $tirage);

                    $nbrTicket = $tirage->getNbrNumero();

                    $nbrTicketGagnant = $tirage->getNbrNumeroGagnant();

                    $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                    //$tirage = $em->getRepository('AppBundle:Ticket')->find($session->get('codeClient'));
                    $prixTicket = $listeTicketsTirage[0]->getTirage()->getCoutTirage();

            if ($request->getMethod() == "GET") {
                if ($status == "ok") {
                    


                    $operation = new Operation();
                    $operation->setMotifOperation('Achat ticket N° ' . $idticket . ' pour le tirage ' . $idtirage . ' a ' . $prixTicket);
                    $operation->setHistoriquePaiement($status);
                    $operation->setMontantOperation($prixTicket);
                    $operation->setClient($client);
                    $operation->setTirage($tirage);
                    $operation->setTypeOperation('Entrée');
                    $em->persist($operation);
                    $em->flush();


                    $clientTicket = $em->getRepository('AppBundle:ClientTicket')->find($listeTicketsTirage[0]->getId());
                    $clientTicket->setTicketAcheter(true);
                    $clientTicket->setClient($client);
                    $clientTicket->setDateAchatTicket(new \DateTime('now'));

                    $em->persist($clientTicket);
                    $em->flush();

                    $nbrTicketRestant = count($em->getRepository('AppBundle:ClientTicket')->findBy(array('tirage' => $tirage, 'ticketAcheter' => false)));
                    if ($nbrTicketRestant == 0) {
                        //dump($nbrTicketRestant);die();
                        $tirage->setEtatTirage('Dépouillement en cours');

                        $listeTicket = [];
                        //$listeTicketGagant = [];

                        for ($i = 1; $i <= $nbrTicket; $i++) {
                            array_push($listeTicket, $i);
                        }


                        $listeTicketGagant = array_rand($listeTicket, $nbrTicketGagnant);

                        for ($j = 0; $j <= $nbrTicketGagnant - 1; $j++) {

                            $detailsTicketsTirage = $em->getRepository('AppBundle:ClientTicket')->findStatutNumberOfTicket($listeTicket[$listeTicketGagant[$j]], $tirage);

                            $ticketTirage = $em->getRepository('AppBundle:ClientTicket')->find($detailsTicketsTirage[0]->getId());

                            $ticketTirage->setTicketGagnant(1);

                            $em->persist($ticketTirage);

                            $em->flush();

                        }

                        $tirage->setEtatTirage('Terminé');

                    } else {
                        $tirage->setEtatTirage('Plus que ' . $nbrTicketRestant . ' disponible');
                    }


                    $em->persist($tirage);
                    $em->flush();


                    $session->getFlashBag()->add('success', 'Ticket achetez avec success !');

                } else {
                    $session->getFlashBag()->add('error', 'Erreur lors du paiement !');

                }

            }

            return $this->render('visitor/payerticket.html.twig', compact('tirage', 'nbrTicketDispo', 'listeTicketsTirage'));
        }



        public function userConnect()
        {
            $session = new Session();
            $connect = $session->get('authenticated');
            if ($connect != true) {
                $url = $this->generateUrl('login');
                $response = new RedirectResponse($url);
                $response->send();
                return;
            } else {
                $em = $this->getDoctrine()->getManager();

                $client = $em->getRepository('AppBundle:Client')->find($session->get('codeClient'));
                $session->set("soldeClient", $client->getSolde());

            }
        }
    }
