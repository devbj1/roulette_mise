<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Tirage
 *
 * @ORM\Table(name="tirage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TirageRepository")
 */
class Tirage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cout_tirage", type="integer")
     */
    private $coutTirage;

    /**
     * @var int
     *
     * @ORM\Column(name="nbr_numero", type="integer")
     */
    private $nbrNumero;

    /**
     * @var int
     *
     * @ORM\Column(name="nbr_numero_gagnant", type="integer")
     */
    private $nbrNumeroGagnant;

    /**
     * @var string
     *
     * @ORM\Column(name="etat_tirage", type="string", length=255)
     */
    private $etatTirage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_tirage", type="datetime")
     */
    private $dateTirage;

    public function __construct()
    {

        $this->dateTirage = new \DateTime('now');
        //$this->nbrNumeroGagnant = $this->nbrNumero*0.4;
        $this->etatTirage = "Ouvert";

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCoutTirage()
    {
        return $this->coutTirage;
    }

    /**
     * @param int $coutTirage
     */
    public function setCoutTirage($coutTirage)
    {
        $this->coutTirage = $coutTirage;
    }

    /**
     * @return int
     */
    public function getNbrNumero()
    {
        return $this->nbrNumero;
    }

    /**
     * @param int $nbrNumero
     */
    public function setNbrNumero($nbrNumero)
    {
        $this->nbrNumero = $nbrNumero;
    }

    /**
     * @return int
     */
    public function getNbrNumeroGagnant()
    {
        return $this->nbrNumeroGagnant;
    }

    /**
     * @param int $nbrNumeroGagnant
     */
    public function setNbrNumeroGagnant($nbrNumeroGagnant)
    {
        $this->nbrNumeroGagnant = $nbrNumeroGagnant;
    }

    /**
     * @return string
     */
    public function getEtatTirage()
    {
        return $this->etatTirage;
    }

    /**
     * @param string $etatTirage
     */
    public function setEtatTirage($etatTirage)
    {
        $this->etatTirage = $etatTirage;
    }

    /**
     * @return \DateTime
     */
    public function getDateTirage()
    {
        return $this->dateTirage;
    }

    /**
     * @param \DateTime $dateTirage
     */
    public function setDateTirage($dateTirage)
    {
        $this->dateTirage = $dateTirage;
    }


}

