<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_payeer", type="string", length=255)
     */
    private $idPayeer;

    /**
     * @var string
     *
     * @ORM\Column(name="pin", type="string", length=255)
     */
    private $pin;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="float")
     */
    private $solde;

    /**
     * @var string
     *
     * @ORM\Column(name="id_parrainage", type="string", length=255)
     */
    private $idParrainage;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client")
     */
    private $parrain;


    public function __construct()
    {
        $this->solde = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIdPayeer()
    {
        return $this->idPayeer;
    }

    /**
     * @param string $idPayeer
     */
    public function setIdPayeer($idPayeer)
    {
        $this->idPayeer = $idPayeer;
    }

    /**
     * @return string
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * @param string $pin
     */
    public function setPin($pin)
    {
        $this->pin = $pin;
    }

    /**
     * @return float
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * @param float $solde
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;
    }

    /**
     * @return string
     */
    public function getIdParrainage()
    {
        return $this->idParrainage;
    }

    /**
     * @param string $idParrainage
     */
    public function setIdParrainage($idParrainage)
    {
        $this->idParrainage = $idParrainage;
    }

    /**
     * @return int
     */
    public function getParrain()
    {
        return $this->parrain;
    }

    /**
     * @param int $parrain
     */
    public function setParrain($parrain)
    {
        $this->parrain = $parrain;
    }



}

