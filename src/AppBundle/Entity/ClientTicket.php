<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientTicket
 *
 * @ORM\Table(name="client_ticket")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientTicketRepository")
 */
class ClientTicket
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_ticket", type="string", length=255)
     */
    private $numeroTicket;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ticket_acheter", type="boolean", nullable=true)
     */
    private $ticketAcheter;


    /**
     * @var boolean
     *
     * @ORM\Column(name="ticket_gagnant", type="boolean", nullable=true)
     */
    private $ticketGagnant;

    /**
     * @var boolean
     *
     * @ORM\Column(name="est_payer", type="boolean", nullable=true)
     */
    private $estPayer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_achat_ticket", type="datetime", nullable=true)
     */
    private $dateAchatTicket;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client")
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tirage")
     */
    private $tirage;



    public function __construct()
    {
        $this->dateAchatTicket = new \DateTime('now');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNumeroTicket()
    {
        return $this->numeroTicket;
    }

    /**
     * @param string $numeroTicket
     */
    public function setNumeroTicket($numeroTicket)
    {
        $this->numeroTicket = $numeroTicket;
    }

    /**
     * @return bool
     */
    public function isTicketAcheter()
    {
        return $this->ticketAcheter;
    }

    /**
     * @param bool $ticketAcheter
     */
    public function setTicketAcheter($ticketAcheter)
    {
        $this->ticketAcheter = $ticketAcheter;
    }

    /**
     * @return bool
     */
    public function isTicketGagnant()
    {
        return $this->ticketGagnant;
    }

    /**
     * @param bool $ticketGagnant
     */
    public function setTicketGagnant($ticketGagnant)
    {
        $this->ticketGagnant = $ticketGagnant;
    }

    /**
     * @return bool
     */
    public function isEstPayer()
    {
        return $this->estPayer;
    }

    /**
     * @param bool $estPayer
     */
    public function setEstPayer($estPayer)
    {
        $this->estPayer = $estPayer;
    }

    /**
     * @return \DateTime
     */
    public function getDateAchatTicket()
    {
        return $this->dateAchatTicket;
    }

    /**
     * @param \DateTime $dateAchatTicket
     */
    public function setDateAchatTicket($dateAchatTicket)
    {
        $this->dateAchatTicket = $dateAchatTicket;
    }

    /**
     * @return int
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param int $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return int
     */
    public function getTirage()
    {
        return $this->tirage;
    }

    /**
     * @param int $tirage
     */
    public function setTirage($tirage)
    {
        $this->tirage = $tirage;
    }


}

