<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Operation
 *
 * @ORM\Table(name="operation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OperationRepository")
 */
class Operation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type_operation", type="string", length=255)
     */
    private $typeOperation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_operation", type="datetime")
     */
    private $dateOperation;

    /**
     * @var int
     *
     * @ORM\Column(name="montant_operation", type="integer")
     */
    private $montantOperation;

    /**
     * @var string
     *
     * @ORM\Column(name="motif_operation", type="string", length=255)
     */
    private $motifOperation;

    /**
     * @var string
     *
     * @ORM\Column(name="historique_paiement", type="string", length=5000)
     */
    private $historiquePaiement;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client")
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tirage")
     */
    private $tirage;


    public function __construct()
    {
        $this->dateOperation = new \DateTime('now');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTypeOperation()
    {
        return $this->typeOperation;
    }

    /**
     * @param string $typeOperation
     */
    public function setTypeOperation($typeOperation)
    {
        $this->typeOperation = $typeOperation;
    }

    /**
     * @return \DateTime
     */
    public function getDateOperation()
    {
        return $this->dateOperation;
    }

    /**
     * @param \DateTime $dateOperation
     */
    public function setDateOperation($dateOperation)
    {
        $this->dateOperation = $dateOperation;
    }

    /**
     * @return int
     */
    public function getMontantOperation()
    {
        return $this->montantOperation;
    }

    /**
     * @param int $montantOperation
     */
    public function setMontantOperation($montantOperation)
    {
        $this->montantOperation = $montantOperation;
    }

    /**
     * @return string
     */
    public function getMotifOperation()
    {
        return $this->motifOperation;
    }

    /**
     * @param string $motifOperation
     */
    public function setMotifOperation($motifOperation)
    {
        $this->motifOperation = $motifOperation;
    }

    /**
     * @return string
     */
    public function getHistoriquePaiement()
    {
        return $this->historiquePaiement;
    }

    /**
     * @param string $historiquePaiement
     */
    public function setHistoriquePaiement($historiquePaiement)
    {
        $this->historiquePaiement = $historiquePaiement;
    }

    /**
     * @return int
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param int $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return int
     */
    public function getTirage()
    {
        return $this->tirage;
    }

    /**
     * @param int $tirage
     */
    public function setTirage($tirage)
    {
        $this->tirage = $tirage;
    }



}
