<?php

namespace AppBundle\Repository;

/**
 * TirageRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TirageRepository extends \Doctrine\ORM\EntityRepository
{


    public function findAllTirageOfNoEqualStatut($statut)
    {
        return $this->_em->createQuery('SELECT t
            FROM AppBundle:Tirage t
            WHERE t.etatTirage != :statut'
        )
            ->setParameter('statut', $statut)
            ->getResult();
    }


    public function findAllTirageOfClient($clientId)
    {
        return $this->_em->createQuery('SELECT DISTINCT t
            FROM AppBundle:Tirage t
            LEFT JOIN AppBundle:ClientTicket ct WITH ct.tirage=t.id
            WHERE ct.client = :idClient'
        )
            ->setParameter('idClient', $clientId)
            ->getResult();
    }


    public function findNumeroPayerTirageOfClient($tirageId, $clientId)
    {
        return $this->_em->createQuery('SELECT ct.numeroTicket,ct.ticketGagnant
            FROM AppBundle:ClientTicket ct
            LEFT JOIN AppBundle:Tirage t WITH t.id=ct.tirage
            WHERE ct.client = :idClient
            AND ct.tirage = :idTirage
            AND ct.ticketAcheter = :etat'
        )
            ->setParameter('idClient', $clientId)
            ->setParameter('idTirage', $tirageId)
            ->setParameter('etat', true)
            ->getResult();
    }


    public function findNumeroGagnantTirageOfClient($tirageId, $clientId)
    {
        return $this->_em->createQuery('SELECT ct.numeroTicket
            FROM AppBundle:ClientTicket ct
            LEFT JOIN AppBundle:Tirage t WITH t.id=ct.tirage
            WHERE ct.client = :idClient
            AND ct.tirage = :idTirage
            AND ct.ticketAcheter = :etat
            AND ct.ticketGagnant = :etat'
        )
            ->setParameter('idClient', $clientId)
            ->setParameter('idTirage', $tirageId)
            ->setParameter('etat', true)
            ->getResult();
    }


    public function findNumeroGagnantTirage($tirageId)
    {
        return $this->_em->createQuery('SELECT ct.numeroTicket
            FROM AppBundle:ClientTicket ct
            LEFT JOIN AppBundle:Tirage t WITH t.id=ct.tirage
            WHERE ct.tirage = :idTirage
            AND ct.ticketAcheter = :etat
            AND ct.ticketGagnant = :etat'
        )
            ->setParameter('idTirage', $tirageId)
            ->setParameter('etat', true)
            ->getResult();
    }


    public function findNumeroGagnantDetailTirage($tirageId)
    {
        return $this->_em->createQuery('SELECT ct.id,ct.numeroTicket,ct.estPayer,cl.idPayeer,t.coutTirage
            FROM AppBundle:ClientTicket ct
            LEFT JOIN AppBundle:Tirage t WITH t.id=ct.tirage
            LEFT JOIN AppBundle:Client cl WITH cl.id=ct.client
            WHERE ct.tirage = :idTirage
            AND ct.ticketAcheter = :etat
            AND ct.ticketGagnant = :etat'
        )
            ->setParameter('idTirage', $tirageId)
            ->setParameter('etat', true)
            ->getResult();
    }
}
