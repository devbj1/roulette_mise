<?php

namespace AppBundle\Repository;

/**
 * OperationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OperationRepository extends \Doctrine\ORM\EntityRepository
{


    public function sumTypeOperation($typeOperation)
    {
        return $this->_em->createQuery('SELECT SUM(op.montantOperation) as totalSum
            FROM AppBundle:Operation op
            WHERE op.typeOperation = :typeop'
        )
            ->setParameter('typeop', $typeOperation)
            ->getResult();
    }


    public function sumTypeOperationClient($typeOperation,$idClient)
    {
        return $this->_em->createQuery('SELECT SUM(op.montantOperation) as totalSum
            FROM AppBundle:Operation op
            WHERE op.typeOperation = :typeop
            AND op.client = :client'
        )
            ->setParameter('typeop', $typeOperation)
            ->setParameter('client', $idClient)
            ->getResult();
    }



    public function findOperationClient($idClient)
    {
        return $this->_em->createQuery('SELECT op
            FROM AppBundle:Operation op
            WHERE op.client = :client
            ORDER BY op.id DESC'
        )
            ->setParameter('client', $idClient)
            ->getResult();
    }



    public function findTypeOperation($typeOperation)
    {
        return $this->_em->createQuery('SELECT cl.idPayeer,op.dateOperation,op.montantOperation
            FROM AppBundle:Operation op
            LEFT JOIN AppBundle:Client cl WITH cl.id=op.client
            WHERE op.typeOperation = :typeop
            ORDER BY op.id DESC'
        )
            ->setMaxResults(5)
            ->setParameter('typeop', $typeOperation)
            ->getResult();
    }

}
