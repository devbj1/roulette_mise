<?php

namespace AjaxBundle\Controller;

use AppBundle\Entity\Operation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/listeticketgagnant", name="ajax_liste_ticket_gagnant")
     * @Method({"GET", "POST"})
     */
    public function listTicketGagnantAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $listeTicketsGagnantTirage = $em->getRepository('AppBundle:Tirage')->findNumeroGagnantDetailTirage($request->get('idTirage'));
        //dump($listeTicketsGagnantTirage);die();
        $listeTicketsGagnant = [];
        foreach ($listeTicketsGagnantTirage as $ticketGagnant) {
            if ($ticketGagnant['estPayer'] == null){
                $listeConsul = [];
                //dump($ticketGagnant);die();
                $listeConsul['id'] = $ticketGagnant['id'];
                $listeConsul['numeroTicket'] = $ticketGagnant['numeroTicket'];
                $listeConsul['idPayeer'] = $ticketGagnant['idPayeer'];
                $listeConsul['montantGagner'] = $ticketGagnant['coutTirage']*2;

                array_push($listeTicketsGagnant, $listeConsul);
            }

        }

        //dump($listeTicketsGagnant);die();
        return $this->render('visitor/listeticketgagnant.html.twig', compact('listeTicketsGagnant'));
        //return new JsonResponse($listeTicketsGagnant);

    }

    /**
     * @Route("/payerticketgagnant", name="ajax_payer_consultation")
     * @Method({"GET", "POST"})
     */
    public function payerTicketGagnantAction(Request $request)
    {

        //("1");die();

       $em = $this->getDoctrine()->getManager();
        $tirageClient = $em->getRepository('AppBundle:ClientTicket')->find($request->get('idTirageClient'));

        if ($tirageClient->isEstPayer() == false)
            {
        $accountNumber = 'P1028537400';
        $apiId = '1059392219';
        $apiKey = '8iuMfg82aSncjMgD';
        $payeer = new \CPayeer($accountNumber, $apiId, $apiKey);
        //dump($payeer->getErrors());die();
        dump($tirageClient->getClient()->getIdPayeer());die();
        
        if ($payeer->isAuth())
        {
         //   floatval($tirageClient->getTirage()->getCoutTirage())*2
            $initOutput = $payeer->initOutput(array(
                'account' => $session->get("idPayeer"),
                 'apiId' => $apiId,
                 'apiPass' => $apiKey,
                 'action' => 'transfer',
                 'm_desc' => ('Paiement de gain du tirage ' . $tirageClient->getTirage()->getId() . ' pour le ticket N°' . $tirageClient->getNumeroTicket() .' dont le coût est '. $tirageClient->getTirage()->getCoutTirage() .'$'),
                 'to' => $session->get("idPayeer"),
                 //'sumIn' => 10,
                 //'curIn' => 'USD',
                 'sumOut' => 0.02,
                 'curOut' => 'USD',
                 'ps' => '1136053',
                //'ps' => '1136053',
                //'sumIn' => 1,
                // 'curIn' => 'USD',
                // 'sumOut' => 1,
                // 'curOut' => 'USD',
                // 'param_ACCOUNT_NUMBER' => 'P1028537400'
            ));

            if ($initOutput)
            {
                $historyId = $payeer->output();
                if ($historyId > 0)
                {
                    
                    $tirageClient->setEstPayer(true);
                    $em->persist($tirageClient);
                    $em->flush();
                    $operation = new Operation();
                    $operation->setMotifOperation('Paiement de gain du tirage ' . $tirageClient->getTirage()->getId() . ' pour le ticket N°' . $tirageClient->getNumeroTicket() .' dont le coût est '. $tirageClient->getTirage()->getCoutTirage() .'$');
                    $operation->setHistoriquePaiement("Recharge");
                    $operation->setMontantOperation(floatval($tirageClient->getTirage()->getCoutTirage())*2);
                    $operation->setClient($tirageClient->getClient());
                    //$operation->setTirage($tirage);
                    $operation->setTypeOperation('Sortie');
                    $em->persist($operation);
                    $em->flush();

                    if ($tirageClient->getId() == null){
                        return new JsonResponse(array(
                            'statut'=>'error'
                        ));
                    }else{
                        return new JsonResponse(array(
                            'statut'=>'success'
                        ));
                    }
                }
                else
                {
                     return new JsonResponse(array(
                            'statut'=>'success'
                        ));
                }
            }
            else
            {
                 return new JsonResponse(array(
                            'statut'=>'success'
                        ));
            }
        }
        else
        {
             return new JsonResponse(array(
                            'statut'=>'success'
                        ));
        }
        }
                else
        {
             return new JsonResponse(array(
                            'statut'=>'Déjà payer'
                        ));
        }

//        $client = $em->getRepository('AppBundle:ClientTicket')->find($listeTicketsTirage[0]->getId());

      


    }

}
